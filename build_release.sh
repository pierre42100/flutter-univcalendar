#!/bin/bash

if [ ! $1 ]
then
	echo "Usage: $0 [flavor_name]"
	exit
fi

BUILD_NAME=$(cat pubspec.yaml  | grep "version: " | cut -d " " -f 2 | cut -d + -f 1)
BUILD_NUMBER=$(cat pubspec.yaml  | grep "version: " | cut -d + -f 2)

echo "Building release for $1"
echo "Build name: $BUILD_NAME"
echo "Build number: $BUILD_NUMBER"

flutter clean

flutter build apk --flavor $1 -t lib/flavors/main_$1.dart --split-per-abi

