# Calendar Application
Calendar client for french universities. Allows to easily get schedules.
The application is written in Dart using Flutter.

# Features
* Offline access to calendar
* Support QrCode scan
* Can customize calendar events colors
* Support both tablets and smartphone
* Works both on Android and (but not tested yet) iOS
* Easily arrange calendar entries size

# Screenshots
| Lyon 1 | Saint-Etienne |
| --     | --            |
|[![Google Play Link](docs/google-play-badge.png)](https://play.google.com/store/apps/details?id=org.communiquons.cal.lyon1)|[![Google Play Link](docs/google-play-badge.png)](https://play.google.com/store/apps/details?id=org.communiquons.cal.stetienne)|
|![Screenshot of the Lyon 1 Application on tablets](docs/screenshot_lyon1_landscape.png)|![Screenshot of the St Etienne Application on tablets](docs/screenshot_stetienne_portrait.png)|

# Current universities supported
* Université Lyon 1
* Université de Saint-Etienne
* Generic iCal application

# Generate releases

## Android
Create a file name `key.properties` in the android directory with the following content:
```
storePassword=store password
keyPassword=key password
keyAlias=key alias
storeFile=path to keystore file

```

Run the following command in Flutter :
* Lyon: `./build_release.sh lyon1`
* Saint-Etienne: `./build_release.sh stetienne`
* Generic ical `./build_release.sh ical`

# Would like to get other universities supported ?
Contact me : pierre.git@communiquons.org

# Author
Pierre HUBERT

# License
GNU GPL v3 License