/**
 * Lyon 1 calendar picker
 *
 * @author Pierre HUBERT
 */

/// Improve interface appearance
function pre_clean_ui() {

    if(document.querySelector(".x-grid3-scroller") == null)
        setTimeout(pre_clean_ui, 1000);
    else
        clean_ui();
}

function clean_ui() {
    document.getElementById("x-auto-13").style.display = "none";
    document.getElementById("x-auto-32").style.display = "none";
    document.querySelector(".x-grid3-scroller").style.overflow = "scroll";

    // Show help notice
    newElem = document.createElement("div");
    newElem.innerHTML = "Veuillez s&eacute;lectionner l'emploi du temps que vous souhaitez installer sur cet appareil et cliquer sur Valider.";
    document.getElementById("x-auto-32").parentNode.appendChild(newElem);
    newElem.style.top = document.getElementById("x-auto-32").style.top;
    newElem.className = document.getElementById("x-auto-32").className;
    newElem.style.color = "white";
    newElem.style.width = "100%";
    newElem.style.fontSize = "small";

}

/// Submit currently selected calendar ID
function do_validate() {
    try {
        var finalURL = "https://thisurlwillneverexists.android.communiquons.org/?id=";

        var calendarID = document.querySelector(".x-panel-body [aria-selected='true'] .x-tree3-node")
                                            .id.split("_").pop();

        if(calendarID.length == 0 || calendarID[0] == "-")
            throw "Invalid calendar number !";

        location.href = finalURL + calendarID;

    } catch (error) {
        alert("Impossible d'extraire le numéro du calendrier choisi!");
    }
}

pre_clean_ui();