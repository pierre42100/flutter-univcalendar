/**
 * St Etienne calendar picker
 *
 * @author Pierre HUBERT
 */

function eventFire(el, etype){
	if (el.fireEvent) {
		el.fireEvent('on' + etype);
	} else {
		var evObj = document.createEvent('Events');
		evObj.initEvent(etype, true, false);
		el.dispatchEvent(evObj);
	}
}

function findBtn(content, content2) {
	var results = document.querySelectorAll("button.x-btn-text");

	for(var i = 0; i < results.length; i++) {
		var btn = results[i];
		if(content && btn.innerHTML == content)
			return btn;

		if(content2 && btn.innerHTML == content2)
			return btn;
	}

	return null;
}



var currentURL = location.href;
var configModeURL = "https://planning.univ-st-etienne.fr/choix_mode_config";

var universityConfiguredCalendar = "https://planning.univ-st-etienne.fr/direct/myplanning.jsp";

var getAllCalendarPageURL = "https://ent.univ-st-etienne.fr/uPortal/f/u411l1s839/normal/render.uP";
var pickCalendarURL = "https://planning.univ-st-etienne.fr/direct/index.jsp";

var finalURL = "https://thisurlwillneverexists.android.communiquons.org/?id=";


// Calendar selection page
if(currentURL.indexOf(configModeURL) == 0) {

    // Update page content
    document.body.innerHTML =
       "<style>body {text-align: center;} button {min-height: 100px; padding-bottom: 10px; width: 100%;}</style>" +
       "<p>Veuillez choisir un mode de configuration de calendrier :</p>" +
       "<p><button onclick='location.href=\""+universityConfiguredCalendar+"\";'>Utiliser le calendrier configuré par l'université</button></p>" +
       "<p><button onclick='location.href=\""+getAllCalendarPageURL+"\";'>Sélectionner un calendrier depuis l'interface web</button></p>";
}


// Use calendar configured by the university
if(currentURL.indexOf(universityConfiguredCalendar) == 0) {
    // Click on export button
    function click_export() {

        if(document.getElementById("x-auto-93") == null) {
            setTimeout(click_export, 1000);
            return;
        }

        setTimeout(function(){
            eventFire(document.getElementById("x-auto-93").querySelector(".x-btn-mc").querySelector("img"), "click");

            generateURL();
        }, 3000);
    }

    // Click on "generate URL" button
    function generateURL() {

        if(findBtn("Générer URL", "Generate URL") == null) {
            setTimeout(generateURL, 1000);
            return;
        }

        setTimeout(function(){
            eventFire(findBtn("Générer URL", "Generate URL"), "click");

            getURL();
        }, 1000);
    }

    // Get URL
    function getURL() {

        if(document.getElementById("logdetail") == null) {
            setTimeout(getURL, 1000);
            return;
        }

        try {

            // Extract URL and parse it
            var url = document.getElementById("logdetail").querySelector("a").href;
            console.log("Extracted URL: " + url);

            if(url.indexOf("resources=") == -1)
              throw("Resource id not found in URL!");

            var calendarID = url.split("resources=")[1].split("&")[0];

            console.log("Extracted resource ID: " + calendarID);

            // Forward final resource ID
            location.href = finalURL + calendarID;


        } catch(error) {
            alert("Erreur lors de la récupération du numéro de calendrier !");
            console.log(error);
        }
    }

    click_export();
}



// Get calendars URL
if(currentURL.indexOf(getAllCalendarPageURL) == 0) {

    setTimeout(function() {

        try {

            // Extract URL from page
            var url = document.querySelector("a[href^='https://planning.univ-st-etienne.fr/direct/index']").href;

            console.log("Found URL: " + url);

            // Redirect page
            location.href = url;


        } catch(error) {
            alert("Erreur lors de la récupération de l'adresse de la page web des calendriers!");
            console.log(error);
        }

    }, 1000);

}


// Pick calendar URL
if(currentURL.indexOf(pickCalendarURL) == 0) {

    /// Improve interface appearance
    function pre_clean_ui() {

        if(document.querySelector(".x-grid3-scroller") == null)
            setTimeout(pre_clean_ui, 1000);
        else
            clean_ui();
    }

    function clean_ui() {
        document.getElementById("x-auto-13").style.display = "none";
        document.getElementById("x-auto-32").style.display = "none";
        document.querySelector(".x-grid3-scroller").style.overflow = "scroll";

        // Show help notice
        newElem = document.createElement("div");
        newElem.innerHTML = "Veuillez s&eacute;lectionner l'emploi du temps que vous souhaitez installer sur cet appareil et cliquer sur Valider.";
        document.getElementById("x-auto-32").parentNode.appendChild(newElem);
        newElem.style.top = document.getElementById("x-auto-32").style.top;
        newElem.className = document.getElementById("x-auto-32").className;
        newElem.style.color = "white";
        newElem.style.width = "100%";
        newElem.style.fontSize = "small";

    }

    /// Submit currently selected calendar ID
    function sub_do_validate() {
        try {

            var calendarID = document.querySelector(".x-panel-body [aria-selected='true'] .x-tree3-node")
                                                .id.split("_").pop();

            if(calendarID.length == 0 || calendarID[0] == "-")
                throw "Invalid calendar number !";

            location.href = finalURL + calendarID;

        } catch (error) {
            alert("Impossible d'extraire le numéro du calendrier choisi!");
        }
    }

    pre_clean_ui();

}



function do_validate() {
    if(currentURL.indexOf(pickCalendarURL) == 0)
        sub_do_validate();
}