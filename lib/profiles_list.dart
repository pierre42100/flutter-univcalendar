import 'dart:collection';

import 'package:univ_calendar_app/app_preferences.dart';

/// Calendar profiles list
///
/// @author Pierre HUBERT

class ProfilesList extends ListBase<CalendarProfile> {
  final _list = <CalendarProfile?>[];

  int get length => _list.length;

  set length(l) => _list.length = l;

  @override
  CalendarProfile operator [](int index) => _list[index]!;

  @override
  void operator []=(int index, CalendarProfile value) => _list[index] = value;

  /// Check whether a profile is present in this list or not
  bool hasProfile(int num) => _list.where((e) => e!.num == num).isNotEmpty;

  /// Find the position of a profile in the list
  int findProfile(int num) => _list.indexWhere((e) => e!.num == num);

  /// Remove a profile from the list
  void removeProfile(int num) => _list.removeWhere((e) => e!.num == num);

  /// Replace a profile with another one
  void replaceProfile(int num, CalendarProfile profile) =>
      _list[findProfile(num)] = profile;

  CalendarProfile getProfile(int num) => _list.firstWhere((e) => e!.num == num)!;

  /// Find & return highest calendar number
  int get highestProfileNum {
    int num = 0;
    forEach((f) => num = f.num > num ? f.num : num);
    return num;
  }

  /// Check out whether a calendar is too old or not
  bool get hasOneCalendarTooOld => where((f) => f.isCalendarTooOld).length > 0;

  /// Get oldest calendar update
  int get oldestCalendarUpdate {
    int max = 0;

    forEach((f) {
      if (f.daysSinceLastUpdate > max) max = f.daysSinceLastUpdate;
    });

    return max;
  }

  /// Get the list of the name of profiles in this list
  List<String> get names => map((f) => f.name).toList();
}
