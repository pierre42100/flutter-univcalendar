import 'dart:ui';

import 'package:univ_calendar_app/calendar_helper.dart';
import 'package:univ_calendar_app/calendar_id.dart';
import 'package:univ_calendar_app/choose_calendar_route.dart';
import 'package:univ_calendar_app/colors_set.dart';

/// Configuration pattern
///
/// @author Pierre HUBERT

late Config _currConfig;

/// Initialize Calendar ID
typedef CalendarIDInitializer = CalendarID Function(String);

/// Initialize a calendar helper
typedef CalendarHelperInitializer = CalendarHelper Function();

/// Initialize a choose calendar route
typedef ChooseCalendarRouteInitializer = ChooseCalendarRoute Function();

class Config {
  /// Application name
  final String appName;

  /// Default colors
  final Color appBarColor;
  final Color defaultTextColor;
  final Color defaultBackgroundColor;

  /// Methods and classes to use
  final CalendarIDInitializer calendarIDInitializer;
  final CalendarHelperInitializer calendarHelperInitializer;
  final ChooseCalendarRouteInitializer chooseCalendarRouteInitializer;

  /// Initialize configuration
  const Config({
    required this.appName,
    required this.appBarColor,
    required this.defaultTextColor,
    required this.defaultBackgroundColor,
    required this.calendarIDInitializer,
    required this.calendarHelperInitializer,
    required this.chooseCalendarRouteInitializer,
  });

  TileColor get defaultTilesColor =>
      TileColor(bg: defaultBackgroundColor, fg: defaultTextColor);

  /// Set global configuration. Note : This has to be made only once !!!
  static void setConfig(Config conf) {
    _currConfig = conf;
  }

  /// Get global configuration
  static Config get() {
    return _currConfig;
  }
}
