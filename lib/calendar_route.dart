import 'dart:async';

import 'package:calendar_view/calendar_view.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
/// Calendar route
///
/// The calendar is actually shown on this file
///
/// @author Pierre HUBERT
import 'package:flutter/material.dart';
import 'package:synchronized/synchronized.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar_cache.dart';
import 'package:univ_calendar_app/calendar_entry.dart';
import 'package:univ_calendar_app/calendar_widget.dart';
import 'package:univ_calendar_app/calendars_list.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/preferences_manager.dart';
import 'package:univ_calendar_app/preferences_route.dart';
import 'package:univ_calendar_app/profiles_list.dart';
import 'package:univ_calendar_app/utils.dart';

import 'color_manager.dart';

enum _CalendarMenuRouteOptions {
  CLOSE_CALENDAR,
  RELOAD_CALENDAR,
  PREFERENCES,
  ABOUT_APPLICATION
}

enum _ProfileMenuOptions { CHANGE_DEFAULT_COLOR, RENAME, DELETE }

enum _ErrorLevel { NONE, MINOR, MAJOR }

const _WhiteTextStyle = TextStyle(color: Colors.white);

class CalendarRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CalendarRouteState();
}

class _CalendarRouteState extends State<CalendarRoute> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  _ErrorLevel _error = _ErrorLevel.NONE;
  bool _loading = true;
  CalendarsList? _calendars;
  DateTime _currentDay = DateTime.now();
  ApplicationPreferences? _preferences;
  GlobalCalendarBounds? _globalCalendarBounds;
  late GlobalCalendarBounds _filteredCalendarBounds;
  final _updateProfileLock = new Lock();
  final _eventsController = EventController<CalendarEntryWithNumber>();

  /// Get current profile, if available
  ProfilesList? get enabledProfiles =>
      _preferences == null ? null : _preferences!.enabledProfiles;

  bool get hasProfiles => enabledProfiles != null;

  /// Choose initial calendar (used if all calendar are closed)
  void _chooseInitialCalendar() async {
    final route = Config.get().chooseCalendarRouteInitializer();

    final navigator = Navigator.of(context);

    // Switch to choose calendar route & wait for calendar to be selected
    navigator.pushReplacement(MaterialPageRoute(builder: (c) => route));
    final id = await route.future;

    // Save calendar & open calendar again
    await PreferencesManager()
        .addProfile(CalendarProfile(num: 0, calendarID: id, name: "EDT"));
    navigator
        .pushReplacement(MaterialPageRoute(builder: (c) => CalendarRoute()));
  }

  /// Add a new profile
  void _addNewProfile() async {
    // Force calendars reload
    setState(() {
      _calendars = null;
    });

    final route = Config.get().chooseCalendarRouteInitializer();

    // Switch to choose calendar route & wait for calendar to be selected
    Navigator.of(context).push(MaterialPageRoute(builder: (c) => route));
    final id = await route.future;
    final num = _preferences!.highestProfilesNumber + 1;

    // Ask user for profile name
    String name = "EDT $num";
    final newName = await pickString(
        context: context,
        title: "Nom de profil",
        description: "Veuillez donner un nom à ce nouveau profil",
        defaultValue: name);
    if (newName != null && newName.length > 0) name = newName;

    // Save calendar & open calendar again
    final newProfile = CalendarProfile(num: num, calendarID: id, name: name);
    await PreferencesManager().addProfile(newProfile);

    Navigator.of(context).pop();

    _setProfileEnabled(profile: newProfile, enable: true);
  }

  /// Let the user choose new default colors for a specified calendar entry
  void _pickProfileDefaultColor(CalendarProfile f) async {
    final newColor = await pickColors(
      context: context,
      currentColors: f.actualDefaultTileColor,
      sampleText: f.name,
    );

    if (newColor == null) return;

    f.mainColor = newColor;
    print("New color: fg=${newColor.fg} bg=${newColor.bg}");
    await PreferencesManager().updateProfile(f);

    setState(() {
      // Nothing
    });
  }

  /// Ask the user about the new name of a profile
  void _renameProfile(CalendarProfile profile) async {
    final newName = await pickString(
      context: context,
      title: "Renommer le profil",
      description: "Veuillez donner un nouveau nom au profil:",
      defaultValue: profile.name,
    );

    if (newName == null || newName.length < 1) return;

    profile.name = newName;
    await PreferencesManager().updateProfile(profile);

    setState(() {
      // Nothing
    });
  }

  /// Ask the user if he is really sure he wants to close the calendar or not
  Future<void> _confirmCloseCalendar(CalendarProfile profileToDelete) async {
    const CONFIRM_ACTION = "Confirm";

    final v = await showDialog<String>(
      context: context,
      builder: (BuildContext c) => AlertDialog(
        title: Text("Confirmer"),
        content: Text("Voulez-vous vraiment fermer ce calendrier ?"),
        actions: <Widget>[
          // Cancel action
          TextButton(
            child: Text("Annuler".toUpperCase()),
            onPressed: () => Navigator.pop(c, "Cancel"),
          ),

          //Confirm action
          TextButton(
            child: Text(
              "Confirmer".toUpperCase(),
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () => Navigator.pop(c, CONFIRM_ACTION),
          ),
        ],
      ),
    );
    if (v == null || v != CONFIRM_ACTION) return;

    //Empty cache and remove calendar number
    CalendarCache().deleteCache(profileToDelete);
    await PreferencesManager().removeProfile(profileToDelete);

    // Restart route - this reset current list of calendars + currently selected
    // calendar
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (c) => CalendarRoute()));
  }

  /// About the application
  Future<void> _aboutApplication(BuildContext context) async {
    showDialog(
        context: context,
        builder: (c) {
          return AboutDialog(
            applicationName: Config.get().appName,
            applicationLegalese: "(c) Pierre HUBERT",
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Cette application est entièrement indépendante, n'est affiliée à aucun organisme.",
                  style: TextStyle(fontSize: 10.0),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Calendrier(s) affiché(s) : " +
                      (hasProfiles
                          ? enabledProfiles!
                              .map((f) => f.calendarID.toString())
                              .join(", ")
                          : "aucun"),
                  textAlign: TextAlign.center,
                ),
              ),
              Text("Support : calendarapps@communiquons.org")
            ],
          );
        });
  }

  /// Method called when the makes a selection on the menu
  void _selectedMenuOption(
      _CalendarMenuRouteOptions option, BuildContext context) {
    // Close calendar
    if (option == _CalendarMenuRouteOptions.CLOSE_CALENDAR)
      _confirmCloseCalendar(enabledProfiles!.first);

    // Reload calendar
    if (option == _CalendarMenuRouteOptions.RELOAD_CALENDAR)
      _loadCalendar(loadCache: false, allowErrorPopup: true);

    // Show application preferences
    if (option == _CalendarMenuRouteOptions.PREFERENCES) _openPreferences();

    // About application
    if (option == _CalendarMenuRouteOptions.ABOUT_APPLICATION)
      _aboutApplication(context);
  }

  /// Change currently selected profile
  Future<void> _setProfileEnabled({
    required CalendarProfile profile,
    required bool enable,
  }) async {
    await _updateProfileLock.synchronized(() async {
      // Do update
      profile.shown = enable;
      await PreferencesManager().updateProfile(profile);

      final completer = Completer<void>();

      setState(() {
        _calendars = null;
        completer.complete();
      });

      await completer.future;

      await _performLoadCalendar();
    });
  }

  /// Refresh preferences
  Future<void> _refreshPreferences() async {
    _preferences = await PreferencesManager().getPreferences();
  }

  /// Refresh calendar bounds
  Future<GlobalCalendarBounds> _refreshFilteredBounds(
      {DateTime? newDay}) async {
    final day = newDay == null ? _currentDay : newDay;

    return await getGlobalCalendarBounds(_preferences!,
        daysToCheck: !showFullWeek(context, _preferences!)
            ? (<DateTime>[]..add(day))
            : getWeekOfDay(day, DaysPreferences.allTrue()),
        calendarsToCheck: enabledProfiles);
  }

  /// Load the calendar
  Future<void> _loadCalendar(
      {bool loadCache = false, bool allowErrorPopup = false}) async {
    try {
      // Load calendar preferences
      await _refreshPreferences();

      // Check if no calendar is currently selected
      if (!_preferences!.hasAtLeastOneCalendar) {
        _chooseInitialCalendar();
        return;
      }

      final list = CalendarsList();

      for (var i = 0; i < enabledProfiles!.length; i++) {
        final profile = enabledProfiles![i];

        // Check if what is requested is impossible
        // (requested to load cache without cache)
        if (loadCache && !await CalendarCache().hasCache(profile)) return;

        // Enter in loading state
        setState(() {
          _error = _ErrorLevel.NONE;
          _loading = true;
        });

        // Load the list from the desired source
        final calendar = loadCache
            ? await CalendarCache().loadCache(profile)
            : await Config.get().calendarHelperInitializer().download(profile);

        if (calendar == null) {
          setState(() {
            _error = _calendars == null ? _ErrorLevel.MAJOR : _ErrorLevel.MINOR;
            _loading = false;
          });
          return;
        }

        // If we refreshed the calendar from an online source, update it
        if (!loadCache) {
          // Merge new entries with current list (if any)
          if (_calendars != null)
            // Get all events previous to today to keep them in cache
            calendar.mergeWith(_calendars![i].getBefore(getTodayAtMidnight()));

          profile.lastUpdate = DateTime.now().millisecondsSinceEpoch;
          await PreferencesManager().updateProfile(profile);

          await CalendarCache().saveCache(profile, calendar);
        }

        // If auto-color is enabled, parse calendar
        if (_preferences!.autoColorCalendar == true) {
          final colorManager = ColorManager();
          await colorManager.initialize();
          await colorManager.fillMissingColorsWithRandom(calendar);
        }

        list.add(calendar);
      }

      // Load global calendar bounds
      final globalBounds = await getGlobalCalendarBounds(_preferences!);
      final filteredBounds = await _refreshFilteredBounds();

      // Notify user
      if (!loadCache && enabledProfiles!.length > 0) {
        ScaffoldMessenger.of(_scaffoldKey.currentContext!)
            .removeCurrentSnackBar();
        ScaffoldMessenger.of(_scaffoldKey.currentContext!)
            .showSnackBar(SnackBar(
          content: Row(
            children: <Widget>[
              Icon(Icons.check),
              Text(
                "Mise à jour de ${enabledProfiles!.names.join(", ")} réussie.",
                softWrap: true,
              ),
            ],
          ),
        ));
      }

      final newEvents = List<CalendarEventData<CalendarEntryWithNumber>>.empty(
          growable: true);

      var id = 0;
      for (final cal in list) {
        for (var event in cal) {
          newEvents.add(CalendarEventData(
              title: event.summary,
              date: event.start,
              endDate: event.end,
              startTime: event.start,
              endTime: event.end,
              event: CalendarEntryWithNumber(event, id)));
        }
        id++;
      }

      // Save calendar information
      setState(() {
        _error = _ErrorLevel.NONE;
        _calendars = list;
        _loading = false;
        _globalCalendarBounds = globalBounds;
        _filteredCalendarBounds = filteredBounds;

        _eventsController.removeWhere((c) => true);
        _eventsController.addAll(newEvents);
      });
    } catch (e, s) {
      print("$e $s");

      setState(() {
        _error = _calendars == null ? _ErrorLevel.MAJOR : _ErrorLevel.MINOR;
        _loading = false;
      });

      if (allowErrorPopup) showLoadingError();
    }
  }

  /// Load the calendar twice : once from the cache and once from the server
  Future<void> _performLoadCalendar() async {
    try {
      await _loadCalendar(loadCache: true);

      if (_calendars == null ||
          await Connectivity().checkConnectivity() != ConnectivityResult.none)
        await _loadCalendar(loadCache: false);
    } catch (e, s) {
      print("$e $s");
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    // Load calendar
    _performLoadCalendar();
  }

  /// Build error card
  Widget _buildMajorErrorCard() {
    return Card(
      color: Colors.red,
      elevation: 10.0,
      child: Container(
        height: 150,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Icon
            Icon(
              Icons.error,
              color: Colors.white,
            ),
            //Message
            Padding(
              padding: EdgeInsets.all(16.0),
              child: Text(
                "Une erreur a survenue lors du chargement du calendrier !",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            //Try again button
            MaterialButton(
              onPressed: _loadCalendar,
              child: Text(
                "Réessayer",
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  /// Build minor error card
  Widget _buildMinorErrorCard() {
    return Card(
      elevation: 5.0,
      color: Colors.red,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: <Widget>[
            Icon(
              Icons.error,
              color: Colors.white,
            ),
            Expanded(
              child: Text(
                "Echec de la mise à jour !",
                style: TextStyle(color: Colors.white),
              ),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  _error = _ErrorLevel.NONE;
                });
              },
              child: Text(
                "Cacher".toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
            ),
            TextButton(
              onPressed: () => _loadCalendar(loadCache: false),
              child: Text(
                "Réessayer".toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: CircularProgressIndicator(color: Config.get().appBarColor),
    );
  }

  void _setDay(DateTime day) async {
    final bounds = await _refreshFilteredBounds(newDay: day);
    setState(() {
      _filteredCalendarBounds = bounds;
      _currentDay = day;
    });
  }

  /// Build the list of profiles
  List<Widget> _buildProfilesList() {
    if (_preferences == null) return [];

    return _preferences!.profiles
        .map((f) => ListTile(
              leading: Checkbox(
                value: f.shown,
                onChanged: (v) => _setProfileEnabled(profile: f, enable: v!),
                activeColor: f.actualDefaultTileColor.bg,
                checkColor: f.actualDefaultTileColor.fg,
              ),
              title: Text(f.name),
              selected: f.shown,
              onTap: () => _setProfileEnabled(profile: f, enable: !f.shown),
              trailing: PopupMenuButton<_ProfileMenuOptions>(
                itemBuilder: (c) => [
                  PopupMenuItem(
                    value: _ProfileMenuOptions.CHANGE_DEFAULT_COLOR,
                    child: Text("Couleurs par défaut"),
                  ),
                  PopupMenuItem(
                    value: _ProfileMenuOptions.RENAME,
                    child: Text("Renommer"),
                  ),
                  PopupMenuItem(
                    value: _ProfileMenuOptions.DELETE,
                    child: Text("Supprimer"),
                  )
                ],
                onSelected: (v) {
                  switch (v) {
                    case _ProfileMenuOptions.CHANGE_DEFAULT_COLOR:
                      _pickProfileDefaultColor(f);
                      break;

                    case _ProfileMenuOptions.RENAME:
                      _renameProfile(f);
                      break;

                    case _ProfileMenuOptions.DELETE:
                      Navigator.of(context).pop();
                      _confirmCloseCalendar(f);
                      break;
                  }
                },
              ),
            ))
        .toList();
  }

  Widget _buildDrawer() {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text(Config.get().appName),
              accountEmail: null,
              currentAccountPicture: CircleAvatar(
                child: Icon(Icons.calendar_today),
              ),
            )
          ]
            ..addAll(_buildProfilesList())

            // Add profile button
            ..add(ListTile(
              leading: Icon(Icons.add),
              title: Text("Ajouter un profil"),
              onTap: () => _addNewProfile(),
            )),
        ),
      ),
    );
  }

  Widget _buildCalendarRouteBody() {
    //In case of error
    if (_error == _ErrorLevel.MAJOR) return _buildMajorErrorCard();

    //Loading mode
    if (_calendars == null) return _buildLoadingWidget();

    //The calendar itself
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            _error == _ErrorLevel.MINOR ? _buildMinorErrorCard() : Container(),
            _buildAdvicesHeader(),
            Expanded(
              child: CalendarWidget(
                calendars: _preferences!.hasShifting
                    ? _calendars!.shift(_preferences!.calendarShifting)
                    : _calendars!,
                profiles: enabledProfiles!,
                onDateChange: _setDay,
                preferences: _preferences!,
                controller: _eventsController,
              ),
            ),
          ],
        ),
        Positioned(
          right: 10.0,
          bottom: 10.0,
          child: _loading ? CircularProgressIndicator() : Container(),
        )
      ],
    );
  }

  /// This widget handle the management of top notifications
  Widget _buildAdvicesHeader() {
    Widget? widget;

    widget = _buildNoCalendarSelectedWarning();

    if (widget == null) widget = _buildOverflowWarningHeader();

    if (widget == null) widget = _buildCalendarDeprecationWarningHeader();

    if (widget == null) widget = _buildHiddenDaysWarningHeader();

    if (widget == null) widget = _buildOptimizationHeader();

    if (widget == null) widget = _buildFullWeekHelperHeader();

    //if (widget == null) widget = _buildAutoColorHintHeader();

    if (widget == null) widget = Container();

    return widget;
  }

  /// This warning appears when no calendar is selected
  Widget? _buildNoCalendarSelectedWarning() {
    if (!hasProfiles || enabledProfiles!.length > 0) return null;

    return _HintHeaderWidget(
      bgColor: Colors.deepOrangeAccent,
      fgColor: Colors.white,
      text: "Aucun calendrier n'est sélectionné !",
      icon: Icons.warning,
      actions: [],
    );
  }

  /// This warning appears only if the calendar view is overflowed
  Widget? _buildOverflowWarningHeader() {
    final bounds = _filteredCalendarBounds.hours;

    if (bounds.isNull) return null;

    if (bounds.min >= _preferences!.firstHour &&
        bounds.max <= _preferences!.lastHour) return null;

    return _HintHeaderWidget(
      bgColor: Colors.red,
      fgColor: Colors.white,
      text: "Des événements sont masqués à cause des paramètres actuels.",
      icon: Icons.warning,
      actions: [
        MaterialButton(
          child: Text(
            "Corriger".toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => _setNewMinMax(
              getLargestHourInterval(_preferences!.minMaxHours, bounds)),
        ),
      ],
    );
  }

  /// Widget that warn the user if some events happens in an hidden day in week view
  Widget? _buildHiddenDaysWarningHeader() {
    if (!showFullWeek(context, _preferences!)) return null;

    final daysToEnable = <int>[];

    // Check if an hidden day of the week contains events
    for (int i = 1; i < 8; i++) {
      if (!_preferences!.daysPreferences.getDay(i)! &&
          _filteredCalendarBounds.days.getDay(i)!) daysToEnable.add(i);
    }

    // Check if no hidden days contains events
    if (daysToEnable.length == 0) return null;

    return _HintHeaderWidget(
      bgColor: Colors.red,
      fgColor: Colors.white,
      text: "Certains jours masqués contiennent des événements",
      icon: Icons.error,
      actions: [
        TextButton(
          child: Text(
            "Corriger".toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => _showDaysInWeekView(daysToEnable),
        )
      ],
    );
  }

  /// Widget that suggests to the user to optimize the size of its calendar
  Widget? _buildOptimizationHeader() {
    if (_globalCalendarBounds == null || _globalCalendarBounds!.isNull)
      return null;

    if (_preferences!.hideOptimizationHint ||
        (_globalCalendarBounds!.hours.min <= _preferences!.firstHour &&
            _globalCalendarBounds!.hours.max >= _preferences!.lastHour))
      return null;

    return _HintHeaderWidget(
      bgColor: Colors.blue,
      fgColor: Colors.white,
      text: "Optimisation de l'affichage disponible",
      icon: Icons.info,
      actions: [
        MaterialButton(
          child: Text(
            "Optimiser".toUpperCase(),
            style: _WhiteTextStyle,
          ),
          onPressed: () => _setNewMinMax(_globalCalendarBounds!.hours),
        ),
        MaterialButton(
          child: Text(
            "Masquer".toUpperCase(),
            style: _WhiteTextStyle,
          ),
          onPressed: () => _hideOptimizationHint(),
        ),
      ],
    );
  }

  /// Widgets that helps the user to get a week view
  Widget? _buildFullWeekHelperHeader() {
    // Continue only if optimisation tips are allowed
    if (_preferences!.hideOptimizationHint) return null;

    // Continue only in landscape mode (optimization)
    if (MediaQuery.of(context).orientation == Orientation.portrait) return null;

    // Continue only if we can not already show the full week
    if (showFullWeek(context, _preferences!)) return null;

    // Continue only if sunday is enabled
    if (!_preferences!.daysPreferences.sunday!) return null;

    // Continue only if the screen is large enough
    if (!canShowFullWeekWithDays(context, 6)) return null;

    // Continue only if there is not any course on sunday
    if (_globalCalendarBounds!.days.getDay(DateTime.sunday)!) return null;

    return _HintHeaderWidget(
      bgColor: Colors.blue,
      fgColor: Colors.white,
      text:
          "En désactivant l'affichage du dimanche, vous pourrez obtenir une vue complète de votre semaine.",
      icon: Icons.info,
      actions: [
        MaterialButton(
          child: Text(
            "Optimiser".toUpperCase(),
            style: _WhiteTextStyle,
          ),
          onPressed: () => _hideSunday(),
        ),
        MaterialButton(
          child: Text(
            "Non merci".toUpperCase(),
            style: _WhiteTextStyle,
          ),
          onPressed: () => _hideOptimizationHint(),
        )
      ],
    );
  }

  /// Widget that suggests to the user to auto-color his calendar
  // ignore: unused_element
  Widget? _buildAutoColorHintHeader() {
    if (_preferences!.hasSetupAutoColor) return null;

    return _HintHeaderWidget(
      bgColor: Colors.blue,
      fgColor: Colors.white,
      text:
          "Souhaitez-vous activer la coloration automatique des événements du calendrier ?",
      icon: Icons.help,
      actions: [
        ElevatedButton(
          child: Text(
            "Oui".toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => _setEnableAutoColor(true),
        ),
        TextButton(
          child: Text(
            "Non".toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => _setEnableAutoColor(false),
        ),
      ],
    );
  }

  /// Widget that warn user if is calendar has not been refreshed for a while
  Widget? _buildCalendarDeprecationWarningHeader() {
    if (_loading || !enabledProfiles!.hasOneCalendarTooOld) return null;

    return _HintHeaderWidget(
      bgColor: Colors.deepOrange,
      fgColor: Colors.white,
      text:
          "Attention, votre calendrier n'a pas été mis à jours depuis ${enabledProfiles!.oldestCalendarUpdate} jours!",
      icon: Icons.warning,
      actions: [
        TextButton(
          child: Text(
            "Rafraîchir".toUpperCase(),
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () => _loadCalendar(loadCache: false),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(hasProfiles
            ? (enabledProfiles!.length > 0
                ? enabledProfiles!.names.join(", ")
                : "Aucun EDT")
            : "Emploi du temps"),
        actions: <Widget>[
          // Change tiles size buttons
          IconButton(
            onPressed: () => _changeZoom(zoomIn: true),
            icon: Icon(Icons.zoom_in),
          ),
          IconButton(
            onPressed: () => _changeZoom(zoomIn: false),
            icon: Icon(Icons.zoom_out),
          ),

          //Menu
          PopupMenuButton<_CalendarMenuRouteOptions>(
            onSelected: (o) => _selectedMenuOption(o, context),
            itemBuilder: (c) => [
              PopupMenuItem<_CalendarMenuRouteOptions>(
                value: _CalendarMenuRouteOptions.RELOAD_CALENDAR,
                child: Text("Rafraîchir"),
              ),
              PopupMenuItem(
                  value: _CalendarMenuRouteOptions.PREFERENCES,
                  child: Text("Préférences")),
              PopupMenuItem<_CalendarMenuRouteOptions>(
                value: _CalendarMenuRouteOptions.ABOUT_APPLICATION,
                child: Text("A propos"),
              ),
            ]..addAll(hasProfiles && enabledProfiles!.length == 1
                ? [
                    PopupMenuItem<_CalendarMenuRouteOptions>(
                      value: _CalendarMenuRouteOptions.CLOSE_CALENDAR,
                      child: Text("Supprimer ce calendrier"),
                      enabled: enabledProfiles!.length > 0,
                    )
                  ]
                : []),
          )
        ],
      ),
      drawer: _buildDrawer(),
      body: _buildCalendarRouteBody(),
    );
  }

  /// Open application preferences
  void _openPreferences() async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (c) => PreferencesRoute()));
  }

  /// Change display layout
  Future<void> _setNewMinMax(MinMaxHours hours) async {
    await PreferencesManager().setFirstHour(hours.min);
    await PreferencesManager().setLastHour(hours.max);
    await _loadCalendar(loadCache: true);
  }

  Future<void> _hideOptimizationHint() async {
    await PreferencesManager().setHideOptimizationHint(true);
    await _loadCalendar();
  }

  Future<void> _changeZoom({bool? zoomIn}) async {
    if (showFullWeek(context, _preferences!))
      await PreferencesManager().setHourHeightFullWeek(
          _preferences!.heightFullWeek + (zoomIn! ? 1 : -1) * 0.1);
    else
      await PreferencesManager().setHourHeightSingleDay(
          _preferences!.heightSingleDay + (zoomIn! ? 1 : -1) * 0.1);

    await _refreshPreferences();

    setState(() {});
  }

  /// Enable / disable auto-coloration
  Future<void> _setEnableAutoColor(bool enable) async {
    await PreferencesManager().setAutoColorStatus(enable);
    await _loadCalendar(loadCache: true);
  }

  /// Hide the sunday
  Future<void> _hideSunday() async {
    _preferences!.daysPreferences.sunday = false;
    await PreferencesManager().setDayPreferences(_preferences!.daysPreferences);
    await _loadCalendar(loadCache: true);
  }

  /// Show some days in week view
  Future<void> _showDaysInWeekView(List<int> days) async {
    final prefs = _preferences!.daysPreferences;
    days.forEach((f) => prefs.setDayEnabled(f, true));
    await PreferencesManager().setDayPreferences(prefs);
    _loadCalendar(loadCache: true);
  }

  /// Show a popup to notify of loading failure
  Future<void> showLoadingError() {
    return showDialog(
        context: context,
        builder: (c) => AlertDialog(
              title: Text("Erreur de chargement"),
              content: Text("Echec du chargement de l'emploi du temps!"),
              actions: [
                MaterialButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                    _performLoadCalendar();
                  },
                  child: Text("Réessayer"),
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Utiliser les anciennes données"),
                )
              ],
            ));
  }
}

/// Helper to easily create hint headers
class _HintHeaderWidget extends StatelessWidget {
  final Color bgColor;
  final Color fgColor;
  final String text;
  final IconData icon;
  final List<Widget> actions;

  const _HintHeaderWidget({
    Key? key,
    required this.bgColor,
    required this.fgColor,
    required this.text,
    required this.icon,
    required this.actions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: bgColor,
      child: Row(
        children: <Widget>[
          Icon(
            icon,
            color: fgColor,
          ),
          Flexible(
            child: Text(
              text,
              style: TextStyle(color: fgColor),
            ),
          ),
        ]..addAll(actions),
      ),
    );
  }
}
