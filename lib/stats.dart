import 'dart:io';

void reportQrScan({required String url}) async {
  try {
    url = Uri.encodeComponent(url);
    final reqURL = "https://univcalreg.communiquons.org/url?u=$url";
    await (await HttpClient().getUrl(Uri.parse(reqURL))).close();
  } catch (e, s) {
    print("Failed to report scanned QrCode URL! $e $s");
  }
}

void reportNewCalendar({required String app, required String cal}) async {
  try {
    app = Uri.encodeComponent(app);
    cal = Uri.encodeComponent(cal);
    final url = "https://univcalreg.communiquons.org/new?app=$app&cal=$cal";
    await (await HttpClient().getUrl(Uri.parse(url))).close();
  } catch (e, s) {
    print("Failed to report new calendar! $e $s");
  }
}
