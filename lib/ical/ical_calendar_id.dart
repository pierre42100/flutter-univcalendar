import 'package:univ_calendar_app/calendar_id.dart';

/// Ical calendar ID
///
/// @author Pierre HUBERT

class IcalCalendarID extends CalendarID {
  final String url;

  const IcalCalendarID({
    required this.url,
  });

  @override
  String toString() => url;

  @override
  String toDisplayString() => url;
}
