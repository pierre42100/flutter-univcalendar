import 'dart:async';

import 'package:flutter/material.dart';
import 'package:univ_calendar_app/calendar_id.dart';
import 'package:univ_calendar_app/choose_calendar_route.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/ical/ical_calendar_id.dart';
import 'package:univ_calendar_app/utils.dart';

/// Ical choose calendar route
///
/// @author Pierre HUBERT

class IcalChooseCalendarRoute extends ChooseCalendarRoute {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text(Config.get().appName),
        ),
        body: _IcalChooseCalendarRoute(
          completer: completer,
        ),
      );
}

class _IcalChooseCalendarRoute extends StatefulWidget {
  final Completer<CalendarID> completer;

  const _IcalChooseCalendarRoute({Key? key, required this.completer})
      : super(key: key);

  @override
  __IcalChooseCalendarRouteState createState() =>
      __IcalChooseCalendarRouteState();
}

class __IcalChooseCalendarRouteState extends State<_IcalChooseCalendarRoute> {
  final controller = TextEditingController();

  bool get _urlIsValid =>
      controller.text.startsWith("http://") ||
      controller.text.startsWith("https://");

  Widget _buildPastURLWidget() {
    return _ConfigurationCard(
      title: "1. Copier-coller l'URL",
      children: [
        TextField(
          controller: controller,
          decoration: InputDecoration(
            alignLabelWithHint: true,
            labelText: "URL du calendrier",
          ),
          onChanged: (s) => setState(() {}),
          onSubmitted: !_urlIsValid ? null : (s) => _submitURLForm(),
        ),
        ElevatedButton(
          onPressed: !_urlIsValid ? null : () => _submitURLForm(),
          child: Text(
            "Valider",
            style: TextStyle(color: Config.get().defaultTextColor),
          ),
          style: ButtonStyle(
            backgroundColor:
            WidgetStateProperty.all(Config.get().appBarColor),
          ),
        )
      ],
    );
  }

  void _submitURLForm() {
    if (!_urlIsValid) return;
    widget.completer.complete(IcalCalendarID(url: controller.text));
  }

  Widget _buildQrCodeWidget() {
    return _ConfigurationCard(title: "2. Scan de QrCode", children: [
      ElevatedButton(
        onPressed: () => _scanQrCode(),
        child: Text(
          "Scanner",
        ),
        style: ElevatedButton.styleFrom(
          backgroundColor: Config.get().appBarColor,
          foregroundColor: Config.get().defaultTextColor,
        ),
      )
    ]);
  }

  void _scanQrCode() async {
    try {
      final qrCode = await scanQrCode(context);

      if (qrCode == null) throw Exception("Could not scan QrCode!");

      widget.completer.complete(IcalCalendarID(url: qrCode));
    } catch (e) {
      print(e.toString());

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content:
              Text("Une erreur a empêché la configuration du calendrier !")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Text(
                "Configuration du calendrier",
                style: TextStyle(fontSize: 24.0),
              ),
            ),
          ),
          Center(
            child: Text(
                "Veuillez vous procurer l'adresse iCal de votre calendrier."),
          ),
          Center(
            child: Text("Deux options s'offrent à vous :"),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildPastURLWidget(),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: _buildQrCodeWidget(),
          )
        ],
      ),
    );
  }
}

class _ConfigurationCard extends StatelessWidget {
  final String title;
  final List<Widget> children;

  const _ConfigurationCard({
    Key? key,
    required this.title,
    required this.children,
  })  : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Column(
        children: <Widget>[
          Text(
            title,
            textAlign: TextAlign.justify,
            style: TextStyle(fontSize: 18.0),
          )
        ]..addAll(children),
      ),
    );
  }
}
