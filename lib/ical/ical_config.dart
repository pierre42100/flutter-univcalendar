import 'package:flutter/widgets.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/ical/ical_calendar_helper.dart';
import 'package:univ_calendar_app/ical/ical_calendar_id.dart';
import 'package:univ_calendar_app/ical/ical_choose_calendar_route.dart';

/// Ical generic configuration
///
/// @author Pierre HUBERT

class IcalConfig extends Config {
  IcalConfig(
      {required String appName,
      required Color appBarColor,
      required Color defaultTextColor,
      required Color defaultBackgroundColor})
      : super(
          appName: appName,
          appBarColor: appBarColor,
          defaultTextColor: defaultTextColor,
          defaultBackgroundColor: defaultBackgroundColor,
          calendarHelperInitializer: () => IcalCalendarHelper(),
          calendarIDInitializer: (s) => IcalCalendarID(url: s),
          chooseCalendarRouteInitializer: () => IcalChooseCalendarRoute(),
        );
}
