import 'dart:convert';
import 'dart:io';

import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar.dart';
import 'package:univ_calendar_app/calendar_entry.dart';
import 'package:univ_calendar_app/calendar_helper.dart';

/// Ical calendar helper
///
/// @author Pierre HUBERT

class IcalCalendarHelper extends CalendarHelper {
  @override
  Future<Calendar?> download(CalendarProfile profile) async {
    try {
      return await downloadURL(profile.calendarID.toString());
    } on Error catch (e) {
      print(e.toString());
      print(e.stackTrace);
      return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  /// Download & parse iCal calendar content
  static Future<Calendar> downloadURL(
    String url, {
    int shifting = 0,
    bool needLangSupport = true,
    bool needFullDaySupport = true,
  }) async {
    // Perform request
    final response = await (await HttpClient().getUrl(Uri.parse(url))).close();

    if (response.statusCode != 200)
      throw Exception(
          "Response status code is invalid ! => ${response.statusCode}");

    final data = await response.transform(utf8.decoder).join();

    return parse(
      calendar: data,
      shifting: shifting,
      needLangSupport: needLangSupport,
      needFullDaySupport: needFullDaySupport,
    );
  }

  /// Parse iCal calendar content
  static Calendar parse({
    required String calendar,
    int shifting = 0,
    bool needLangSupport = true,
    bool needFullDaySupport = true,
  }) {
    int timeBeginParsing = DateTime.now().microsecondsSinceEpoch;

    // Remove all translation information
    if (needLangSupport)
      calendar = calendar.replaceAll(RegExp(r';LANGUAGE=\w\w:'), ":");

    //Parse the calendar
    int pos = 0;
    Calendar list = Calendar();
    while (calendar.indexOf("BEGIN:VEVENT", pos) != -1) {
      final endPos = calendar.indexOf("END:VEVENT", pos) + 1;

      list.add(CalendarEntry(
          _getDate(calendar, pos, endPos, "DTSTART", shifting: shifting)!,
          _getDate(calendar, pos, endPos, "DTEND", shifting: shifting)!,
          _extractString(calendar, pos, "SUMMARY"),
          _extractString(calendar, pos, "LOCATION"),
          _extractString(calendar, pos, "DESCRIPTION")));

      pos = endPos;
    }

    list.sort();

    print(
        "Time of calendar parsing: ${DateTime.now().microsecondsSinceEpoch - timeBeginParsing} microseconds");

    return list;
  }

  /// Extract a value from the calendar file
  ///
  /// [str] is the calendar file
  /// [pos] is the current position in the file
  /// [key] is the key to retrieve from the file
  ///
  /// Returns the matching key or an empty string if it
  /// was not found
  static String _extractString(String str, int pos, String key) {
    //Search the string
    key = key.toUpperCase() + ":";
    int begin = str.indexOf(key, pos);

    if (begin == -1) return "";

    begin = begin + key.length;
    final int end = str.indexOf("\n", begin);

    if (end == -1) return "";

    return str
        .substring(begin, end)
        .replaceAll("\\n", "\n")
        .replaceAll("\n\n", "\n")
        .replaceAll("\\", "");
  }

  /// Extract a string from the calendar file
  ///
  /// Returns matching date time or null in case of failure
  static DateTime? _getDate(
    String str,
    int pos,
    int endPos,
    String key, {
    int shifting = 0,
    bool needFullDaySupport = true,
  }) {
    // First check if it is a full day date (only if required)
    if (needFullDaySupport) {
      final fullDaySpec = "$key;VALUE=DATE:";
      final fullDaySpecsPos = str.indexOf(fullDaySpec, pos);
      if (fullDaySpecsPos > pos && fullDaySpecsPos < endPos) {
        final extractionPoint = fullDaySpecsPos + fullDaySpec.length;
        final date = str.substring(extractionPoint, extractionPoint + 11);
        return DateTime(
          int.parse(date.substring(0, 4)),
          int.parse(date.substring(4, 6)),
          int.parse(date.substring(6, 8)),
        );
      }
    }

    // Else treat it as a normal event
    final value = _extractString(str, pos, key);

    if (value.isEmpty) return null;

    final hours = (int.parse(value.substring(9, 11)) + shifting).toString();
    return DateTime.parse(value.substring(0, 9) +
            (hours.length == 1 ? "0" + hours : hours) +
            value.substring(11, 16))
        .toLocal();
  }
}
