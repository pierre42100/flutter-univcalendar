/// Main application file
///
/// @author Pierre HUBERT
import 'package:calendar_view/calendar_view.dart';
import 'package:flutter/material.dart';
import 'package:univ_calendar_app/calendar_route.dart';
import 'package:univ_calendar_app/config.dart';

///
/// @author Pierre HUBERT

void subMain() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(CalendarApp());
}

class CalendarApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CalendarAppState();
}

class _CalendarAppState extends State<CalendarApp> {
  @override
  Widget build(BuildContext context) {
    return CalendarControllerProvider(
        controller: EventController(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,

          // Show home screen by default
          home: CalendarRoute(),

          theme: ThemeData(
            appBarTheme: AppBarTheme(
              backgroundColor: Config.get().appBarColor,
                foregroundColor: Colors.white
            ),
            primaryColor: Config.get().defaultBackgroundColor,
            colorScheme: ColorScheme.fromSwatch(
              accentColor:  Config.get().appBarColor,
              backgroundColor: Colors.white,
            )
          ),
        ));
  }
}
