import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar.dart';
import 'package:univ_calendar_app/calendar_cache.dart';
import 'package:univ_calendar_app/colors_set.dart';
import 'package:univ_calendar_app/config.dart';

/// Project utilities
///
/// @author Pierre HUBERT

/// Get a pretty date
String getNiceDate(
  DateTime date, {
  bool days= true,
  bool month= true,
}) {
  assert(days == true || month == true);

  final day = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
    "Dimanche"
  ][date.weekday];

  final monthStr = [
    "",
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre"
  ][date.month];

  if (days && month)
    return "$day ${date.day} $monthStr";
  else if (days)
    return "$day ${date.day}";
  else
    return monthStr.toUpperCase();
}

/// Check out whether a full week should be shown or not
bool showFullWeek(BuildContext context, ApplicationPreferences prefs) {
  return canShowFullWeekWithDays(
      context, prefs.daysPreferences.numberEnabledDays);
}

/// Check if it would be possible to use week layout with a certain number of days
bool canShowFullWeekWithDays(BuildContext context, int numberDays) {
  return MediaQuery.of(context).size.width > 100 * numberDays;
}

/// Get all the days of a week
List<DateTime> getWeekOfDay(DateTime date, DaysPreferences prefs) {
  final firstDay = date.subtract(Duration(days: date.weekday - 1));

  List<DateTime> days = [];

  if (prefs.monday!) days.add(firstDay);
  if (prefs.tuesday!) days.add(firstDay.add(Duration(days: 1)));
  if (prefs.wednesday!) days.add(firstDay.add(Duration(days: 2)));
  if (prefs.thursday!) days.add(firstDay.add(Duration(days: 3)));
  if (prefs.friday!) days.add(firstDay.add(Duration(days: 4)));
  if (prefs.saturday!) days.add(firstDay.add(Duration(days: 5)));
  if (prefs.sunday!) days.add(firstDay.add(Duration(days: 6)));

  return days;
}

/// Get the largest hours interval
MinMaxHours getLargestHourInterval(MinMaxHours one, MinMaxHours two) {
  return MinMaxHours(
    min: one.min < two.min ? one.min : two.min,
    max: one.max > two.max ? one.max : two.max,
  );
}

/// Get a DateTime object pointing at today at 00:00
DateTime getTodayAtMidnight() {
  var today = DateTime.now();
  today = today.subtract(Duration(
      hours: today.hour,
      minutes: today.minute,
      seconds: today.second,
      milliseconds: today.millisecond,
      microseconds: today.microsecond));
  return today;
}

/// Ask the user to input a string
Future<String?> pickString({
  required BuildContext context,
  required String title,
  required String description,
  String defaultValue = "",
}) async {
  final controller = TextEditingController();
  controller.text = defaultValue;

  final accept = await showDialog<bool>(
    context: context,
    builder: (c) => AlertDialog(
      title: Text(title),
      content: Container(
        height: 100,
        child: Column(
          children: <Widget>[
            Text(description),
            TextField(
              controller: controller,
            )
          ],
        ),
      ),
      actions: <Widget>[
        MaterialButton(
            child: Text("Annuler".toUpperCase()),
            onPressed: () => Navigator.of(c).pop(false)),
        MaterialButton(
            child: Text("Ok".toUpperCase()),
            onPressed: () => Navigator.of(c).pop(true)),
      ],
    ),
  );

  if (accept != true) return null;

  return controller.text;
}

/// Get global calendar bounds (min max hours + enabled days)
Future<GlobalCalendarBounds> getGlobalCalendarBounds(
  ApplicationPreferences prefs, {
  List<DateTime>? daysToCheck,
  List<CalendarProfile>? calendarsToCheck,
}) async {
  int min = -1;
  int max = -1;
  final days = DaysPreferences.allFalse();

  for (var p in prefs.profiles) {
    // Check if we can skip calendar
    if (calendarsToCheck != null && !calendarsToCheck.contains(p)) continue;

    // Skip empty calendars
    if (!await CalendarCache().hasCache(p)) continue;

    // Get calendar & filter it if required
    final fullCalendar = (await CalendarCache().loadCache(p));
    final calendar = Calendar();
    if (daysToCheck != null)
      daysToCheck.forEach((d) => calendar.addAll(fullCalendar.getOfDay(d)));
    else
      calendar.addAll(fullCalendar);

    // Process hours
    final firstHour = calendar.firstHour(-1);
    final lastHour = calendar.lastHour(-1);

    if (min == -1 || (firstHour >= 0 && firstHour < min)) min = firstHour;

    if (max == -1 || (lastHour >= 0 && lastHour > max)) max = lastHour;

    // Process days
    for (int i = 1; i < 8; i++) {
      if (!days.getDay(i)! && calendar.hasWeekDay(i))
        days.setDayEnabled(i, true);
    }
  }

  return GlobalCalendarBounds(
      hours: MinMaxHours(min: min, max: max), days: days);
}

/// Change a color
Future<Color?> pickColor(Color current, BuildContext context) async {
  return await showDialog<Color>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Choix de la couleur"),
          content: MaterialPicker(
            pickerColor: current,
            onColorChanged: (c) => Navigator.of(context).pop(c),
          ),
          actions: <Widget>[
            new TextButton(
              onPressed: () => Navigator.of(context).pop(null),
              child: Text("Annuler"),
            ),
          ],
        );
      });
}

/// Pick colors for tile

enum _TileColorChoicePicker { NOTHING, RESET, FOREGROUND, BACKGROUND }

Future<TileColor?> pickColors({
  required BuildContext context,
  required TileColor currentColors,
  required String sampleText,
}) async {
  final choice = await showDialog<_TileColorChoicePicker>(
    context: context,
    builder: (c) {
      return AlertDialog(
        title: Text("Personnalisation"),
        content: Text(sampleText),
        actions: <Widget>[
          TextButton(
            child: Text(
              "Reset".toUpperCase(),
              style: TextStyle(color: Colors.red),
            ),
            onPressed: () => Navigator.of(c).pop(_TileColorChoicePicker.RESET),
          ),
          TextButton(
            child: Text("Texte".toUpperCase()),
            onPressed: () =>
                Navigator.of(c).pop(_TileColorChoicePicker.FOREGROUND),
          ),
          TextButton(
            child: Text("Fond".toUpperCase()),
            onPressed: () =>
                Navigator.of(c).pop(_TileColorChoicePicker.BACKGROUND),
          ),
        ],
      );
    },
  );

  if (choice == null) return null;

  switch (choice) {
    case _TileColorChoicePicker.RESET:
      return TileColor(bg: null, fg: null);

    case _TileColorChoicePicker.FOREGROUND:
      final color = await pickColor(
          currentColors.fg ?? Config.get().defaultTextColor, context);
      return TileColor(
          bg: currentColors.bg, fg: color == null ? currentColors.fg : color);

    case _TileColorChoicePicker.BACKGROUND:
      final color = await pickColor(
          currentColors.fg ?? Config.get().defaultBackgroundColor, context);
      return TileColor(
          bg: color == null ? currentColors.bg : color, fg: currentColors.fg);

    case _TileColorChoicePicker.NOTHING:
      return null;
  }

  // ignore: dead_code
  throw "Reached unreachable statement !";
}

/// Scan a QrCode
Future<String?> scanQrCode(BuildContext context) async {
  final res = await FlutterBarcodeScanner.scanBarcode(
      '#ff6666', "Annuler", true, ScanMode.QR);
  print(res);

  if(res == "")
    return null;

  return res;
}

/// Show a popup
Future<void> showPopup(BuildContext context, String msg) async {
  showDialog(
      context: context,
      builder: (c) => AlertDialog(
            title: Text("Message"),
            content: Text(msg),
            actions: [
              TextButton(
                  onPressed: () => Navigator.of(c).pop(), child: Text("OK"))
            ],
          ));
}
