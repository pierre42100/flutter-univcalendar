/// Single calendar event
///
/// @author Pierre HUBERT

class CalendarEntry implements Comparable<CalendarEntry> {
  final DateTime start;
  final DateTime end;
  final String summary;
  final String location;
  final String description;

  CalendarEntry(
      this.start, this.end, this.summary, this.location, this.description)
      ;

  CalendarEntry.shifted(CalendarEntry entry, int shift)
      : start = entry.start.add(Duration(hours: shift)),
        end = entry.end.add(Duration(hours: shift)),
        summary = entry.summary,
        location = entry.location,
        description = entry.description;

  @override
  int compareTo(CalendarEntry other) {
    return this.start.compareTo(other.start);
  }

  bool equals(CalendarEntry other) => compareTo(other) == 0;

  CalendarEntry.fromJSON(Map<String, dynamic> json)
      : start = DateTime.parse(json["start"]),
        end = DateTime.parse(json["end"]),
        summary = json["summary"],
        location = json["location"],
        description = json["description"];

  Map<String, dynamic> toJSON() {
    return {
      "start": start.toString(),
      "end": end.toString(),
      "summary": summary,
      "location": location,
      "description": description
    };
  }

  Duration get duration => end.difference(start);
}

class CalendarEntryWithNumber {
  final CalendarEntry entry;
  final int number;

  CalendarEntryWithNumber(this.entry, this.number);
}