import 'package:flutter/material.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/color_manager.dart';
import 'package:univ_calendar_app/preferences_manager.dart';

/// Preferences route
///
/// @author Pierre HUBERT

class PreferencesRoute extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PreferencesRouteState();
}

class _PreferencesRouteState extends State<PreferencesRoute> {
  final PreferencesManager _preferencesManager = PreferencesManager();
  ApplicationPreferences? _applicationPreferences;
  TextEditingController firstHourController = TextEditingController();
  TextEditingController lastHourController = TextEditingController();
  TextEditingController singleDayHeightController = TextEditingController();
  TextEditingController fullWeekHeightController = TextEditingController();
  TextEditingController shiftCalendarController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Préférences"),
      ),
      body: _applicationPreferences == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : RefreshIndicator(
              onRefresh: _loadPreferences,
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: _buildPreferencesList(),
                ),
              ),
            ),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _loadPreferences();
  }

  /// Load the list of preferences
  Future<void> _loadPreferences() async {
    setState(() => _applicationPreferences = null);
    final pref = await _preferencesManager.getPreferences();
    setState(() {
      _applicationPreferences = pref;
      firstHourController.text = _applicationPreferences!.firstHour.toString();
      lastHourController.text = _applicationPreferences!.lastHour.toString();
      singleDayHeightController.text =
          _applicationPreferences!.heightSingleDay.toString();
      fullWeekHeightController.text =
          _applicationPreferences!.heightFullWeek.toString();
      shiftCalendarController.text =
          _applicationPreferences!.calendarShifting.toString();
    });
  }

  /// Update a preference
  Future<void> _updatePreference(Future<bool> Function() update,
      {bool needReload = false}) async {
    await update();
    if (needReload) await _loadPreferences();
  }

  /// Update a day preference
  Future<void> _updateDayPreference(
      void Function(DaysPreferences) updateDay) async {
    updateDay(_applicationPreferences!.daysPreferences);
    await _preferencesManager
        .setDayPreferences(_applicationPreferences!.daysPreferences);
    await _loadPreferences();
  }

  Widget _buildTextNumberInput<T>(
      {required String label,
      required TextEditingController controller,
      required bool Function(T) checkValue,
      required void Function(T) onUpdated}) {
    return TextField(
      controller: controller,
      keyboardType: TextInputType.numberWithOptions(
        signed: false,
        decimal: false,
      ),
      decoration: InputDecoration(
        alignLabelWithHint: true,
        labelText: label,
      ),
      onChanged: (s) {
        var value;

        if (T == int)
          value = int.tryParse(s);
        else if (T == double) value = double.tryParse(s);

        if (value != null && checkValue(value)) onUpdated(value);
      },
    );
  }

  Widget _buildCheckBoxInput<T>({
    required String label,
    required void Function(bool?) onUpdate,
    required bool? checked,
  }) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: checked,
          onChanged: onUpdate,
        ),
        Text(label)
      ],
    );
  }

  Widget _buildPreferencesList() {
    return Column(
      children: <Widget>[
        // First Hour
        _buildTextNumberInput<int>(
            label: "Première heure",
            controller: firstHourController,
            onUpdated: (i) =>
                _updatePreference(() => _preferencesManager.setFirstHour(i)),
            checkValue: (v) => v >= 0 && v < _applicationPreferences!.lastHour),

        // Last hour
        _buildTextNumberInput<int>(
            label: "Dernière heure",
            controller: lastHourController,
            onUpdated: (i) =>
                _updatePreference(() => _preferencesManager.setLastHour(i)),
            checkValue: (v) => v > 0 && v > _applicationPreferences!.firstHour),

        // Height for single day view
        _buildTextNumberInput<double>(
            label: "Taille des heures vue d'un jour (défaut: 0.5)",
            controller: singleDayHeightController,
            onUpdated: (i) => _updatePreference(
                () => _preferencesManager.setHourHeightSingleDay(i)),
            checkValue: (v) => v > 0 && v < 3),

        // Height for full week day view
        _buildTextNumberInput<double>(
            label: "Taille des heures vue d'une semaine (défaut: 1)",
            controller: fullWeekHeightController,
            onUpdated: (i) => _updatePreference(
                () => _preferencesManager.setHourHeightFullWeek(i)),
            checkValue: (v) => v > 0 && v < 3),

        // Auto-color calendar
        _buildCheckBoxInput(
            label: "Colorer automatiquement le calendrier",
            onUpdate: (enable) => _updatePreference(
                () => _preferencesManager.setAutoColorStatus(enable!),
                needReload: true),
            checked: _applicationPreferences!.hasSetupAutoColor
                ? _applicationPreferences!.autoColorCalendar
                : false),

        // Hide optimization preference
        _buildCheckBoxInput(
            label: "Masquer les conseils d'optimisation",
            onUpdate: (enable) => _updatePreference(
                () => _preferencesManager.setHideOptimizationHint(enable!),
                needReload: true),
            checked: _applicationPreferences!.hideOptimizationHint),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text("Jours à afficher dans la vue par semaine :"),
        ),

        _buildCheckBoxInput(
            label: "Lundi",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.monday = enable),
            checked: _applicationPreferences!.daysPreferences.monday),

        _buildCheckBoxInput(
            label: "Mardi",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.tuesday = enable),
            checked: _applicationPreferences!.daysPreferences.tuesday),

        _buildCheckBoxInput(
            label: "Mercredi",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.wednesday = enable),
            checked: _applicationPreferences!.daysPreferences.wednesday),

        _buildCheckBoxInput(
            label: "Jeudi",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.thursday = enable),
            checked: _applicationPreferences!.daysPreferences.thursday),

        _buildCheckBoxInput(
            label: "Vendredi",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.friday = enable),
            checked: _applicationPreferences!.daysPreferences.friday),

        _buildCheckBoxInput(
            label: "Samedi",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.saturday = enable),
            checked: _applicationPreferences!.daysPreferences.saturday),

        _buildCheckBoxInput(
            label: "Dimanche",
            onUpdate: (enable) =>
                _updateDayPreference((d) => d.sunday = enable),
            checked: _applicationPreferences!.daysPreferences.sunday),

        _buildTextNumberInput<int>(
            label: "Décaler le calendrier en heures. Défaut : 0",
            controller: shiftCalendarController,
            checkValue: (v) => v > -5 && v < 5,
            onUpdated: (i) => _updatePreference(
                () => _preferencesManager.setCalendarShifting(i))),

        MaterialButton(
          child: Text("Supprimer toutes les couleurs du calendrier"),
          textColor: Colors.white,
          color: Colors.red,
          onPressed: () => _deleteAllColors(),
        ),
      ],
    );
  }

  /// Ask the user if he really wants to delete all colors or not
  Future<void> _deleteAllColors() async {
    final result = await showDialog<bool>(
      context: context,
      builder: (c) => AlertDialog(
        title: Text("Supprimer toute les couleurs"),
        content: Text(
            "Souhaitez-vous supprimer toutes les couleurs personnalisées du calendrier ? L'opération est irréversible !"),
        actions: <Widget>[
          TextButton(
            child: Text("Annuler".toUpperCase()),
            onPressed: () => Navigator.of(c).pop(false),
          ),
          TextButton(
            child: Text("Confirmer".toUpperCase(), style: TextStyle(color: Colors.red),),
            onPressed: () => Navigator.of(c).pop(true),
          ),
        ],
      ),
    );

    if (result != true) return;

    await ColorManager().reset();
  }
}
