import 'dart:convert';
import 'dart:core';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/profiles_list.dart';

/// Preferences manager
///
/// @author Pierre HUBERT

class PreferencesManager {
  /// Get the list of calendar profiles
  Future<ProfilesList> getProfiles() async {
    try {
      final profilesJSON = (await _getString("profiles", "[]"))!;
      print("Loaded profiles: $profilesJSON");
      List list = jsonDecode(profilesJSON);

      return ProfilesList()
        ..addAll(list.map((e) => CalendarProfile.fromJSON(e)).toList());
    } on Error catch (e) {
      print(e);
      print(e.stackTrace);
      print("Could not read calendar profiles !");
    } catch (e) {
      print(e);
      print("Could not read calendar profiles ! (2)");
    }

    return ProfilesList();
  }

  /// Add a new profile to the list
  Future<void> addProfile(CalendarProfile profile) async {
    _saveProfilesList((await getProfiles())..add(profile));
  }

  Future<void> updateProfile(CalendarProfile profile) async {
    print("Update calendar profile: ${profile.toJSON()}");
    _saveProfilesList(await getProfiles()
      ..replaceProfile(profile.num, profile));
  }

  /// Remove a profile from the list
  Future<void> removeProfile(CalendarProfile profile) async {
    _saveProfilesList((await getProfiles())..removeProfile(profile.num));
  }

  /// Save a new list of profiles
  Future<bool> _saveProfilesList(ProfilesList list) async {
    final outStr = jsonEncode(list.map((f) => f.toJSON()).toList());
    print("Save profiles list: $outStr");
    return await _setString("profiles", outStr);
  }

  /// Get & set auto-color preferences
  Future<bool?> getAutoColorStatus() async {
    return await _getBool("auto_color", null);
  }

  Future<bool> setAutoColorStatus(bool status) async {
    return await _setBool("auto_color", status);
  }

  /// Get the first hour to display on the calendar
  Future<int?> getFirstHour() async {
    return await _getInt("first_hour", 0);
  }

  Future<bool> setFirstHour(int firstHour) async {
    return await (await SharedPreferences.getInstance())
        .setInt("first_hour", firstHour);
  }

  /// Get the last hour to show on the calendar
  Future<int?> getLastHour() async {
    return await _getInt("last_hour", 24);
  }

  Future<bool> setLastHour(int lastHour) async {
    return await _setInt("last_hour", lastHour);
  }

  /// Get the space dedicated to each hour
  Future<double?> getHourHeightSingleDay() async {
    return await _getDouble("hour_height_single_day", 0.5);
  }

  Future<bool> setHourHeightSingleDay(double height) async {
    if (height > 3)
      height = 3;
    else if (height < 0.1) height = 0.1;
    return await _setDouble("hour_height_single_day", height);
  }

  /// Get the space dedicated to each hour
  Future<double?> getHourHeightFullWeek() async {
    return await _getDouble("hour_height_full_week", 1);
  }

  Future<bool> setHourHeightFullWeek(double height) async {
    if (height > 3)
      height = 3;
    else if (height < 0.1) height = 0.1;
    return await _setDouble("hour_height_full_week", height);
  }

  Future<bool> setHideOptimizationHint(bool hide) async {
    return await _setBool("hide_optimization_hint", hide);
  }

  /// Get and set day preferences
  Future<DaysPreferences> getDayPreferences() async {
    if (!(await SharedPreferences.getInstance()).containsKey("days_preference"))
      return DaysPreferences();
    else {
      List<String> days = (await SharedPreferences.getInstance())
          .getStringList("days_preference")!;
      return DaysPreferences(
        monday: days.contains("monday"),
        tuesday: days.contains("tuesday"),
        wednesday: days.contains("wednesday"),
        thursday: days.contains("thursday"),
        friday: days.contains("friday"),
        saturday: days.contains("saturday"),
        sunday: days.contains("sunday"),
      );
    }
  }

  Future<bool> setDayPreferences(DaysPreferences preferences) async {
    List<String> days = [];
    if (preferences.monday!) days.add("monday");
    if (preferences.tuesday!) days.add("tuesday");
    if (preferences.wednesday!) days.add("wednesday");
    if (preferences.thursday!) days.add("thursday");
    if (preferences.friday!) days.add("friday");
    if (preferences.saturday!) days.add("saturday");
    if (preferences.sunday!) days.add("sunday");

    return (await SharedPreferences.getInstance())
        .setStringList("days_preference", days);
  }

  /// Get and set calendar shifting
  Future<int?> getCalendarShifting() async {
    return _getInt("shift_calendar", 0);
  }

  Future<bool> setCalendarShifting(int shift) async {
    return _setInt("shift_calendar", shift);
  }

  /// Get all the preferences of the application
  Future<ApplicationPreferences> getPreferences() async {
    return ApplicationPreferences(
      profiles: await getProfiles(),
      autoColorCalendar: await getAutoColorStatus(),
      firstHour: (await getFirstHour())!,
      lastHour: (await getLastHour())!,
      heightSingleDay: (await getHourHeightSingleDay())!,
      heightFullWeek: (await getHourHeightFullWeek())!,
      daysPreferences: await getDayPreferences(),
      hideOptimizationHint: (await _getBool("hide_optimization_hint", false))!,
      calendarShifting: (await getCalendarShifting())!,
    );
  }

  /// Get a string from preferences manager
  Future<String?> _getString(String prefName, String fallback) async {
    if (!(await SharedPreferences.getInstance()).containsKey(prefName))
      return fallback;
    else
      return (await SharedPreferences.getInstance()).getString(prefName);
  }

  /// Set a string to preferences manager
  Future<bool> _setString(String prefName, String value) async {
    return (await SharedPreferences.getInstance()).setString(prefName, value);
  }

  /// Get an int from preferences manager
  Future<int?> _getInt(String prefName, int fallback) async {
    if (!(await SharedPreferences.getInstance()).containsKey(prefName))
      return fallback;
    else
      return (await SharedPreferences.getInstance()).getInt(prefName);
  }

  /// Set an int to preferences manager
  Future<bool> _setInt(String prefName, int value) async {
    return (await SharedPreferences.getInstance()).setInt(prefName, value);
  }

  /// Get a double from preferences manager
  Future<double?> _getDouble(String prefName, double fallback) async {
    if (!(await SharedPreferences.getInstance()).containsKey(prefName))
      return fallback;
    else
      return (await SharedPreferences.getInstance()).getDouble(prefName);
  }

  /// Set a double to preferences manager
  Future<bool> _setDouble(String prefName, double value) async {
    return (await SharedPreferences.getInstance()).setDouble(prefName, value);
  }

  /// Get a boolean from preferences manager
  Future<bool?> _getBool(String prefName, bool? fallback) async {
    if (!(await SharedPreferences.getInstance()).containsKey(prefName))
      return fallback;
    else
      return (await SharedPreferences.getInstance()).getBool(prefName);
  }

  /// Set a boolean to preferences manager
  Future<bool> _setBool(String prefName, bool value) async {
    return (await SharedPreferences.getInstance()).setBool(prefName, value);
  }
}
