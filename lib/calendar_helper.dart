import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar.dart';

abstract class CalendarHelper {
  /// Get the list of calendars
  Future<Calendar?> download(CalendarProfile calendarProfile);
}
