import 'dart:convert';
import 'dart:io';

/// Calendar cache
///
/// @author Pierre HUBERT

import 'package:path_provider/path_provider.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar.dart';
import 'package:univ_calendar_app/calendar_entry.dart';

const _cacheFile = "calendar_cache.json";

class CalendarCache {
  /// Get cache file
  Future<File> _getCacheFile(CalendarProfile profile) async {
    final appDirectory = await getApplicationDocumentsDirectory();
    return File(appDirectory.path + "/${profile.num}-$_cacheFile");
  }

  /// Check out whether a cached version of the calendar is available or not
  Future<bool> hasCache(CalendarProfile profile) async {
    return (await _getCacheFile(profile)).exists();
  }

  /// Delete cached version of the file
  Future<void> deleteCache(CalendarProfile profile) async {
    final file = await _getCacheFile(profile);
    if (await file.exists()) await file.delete();
  }

  /// Load cached version of the calendar
  Future<Calendar> loadCache(CalendarProfile profile) async {
    try {
      final file = await _getCacheFile(profile);
      final cacheMap = jsonDecode(file.readAsStringSync());

      //Parse calendar
      Calendar calendar = Calendar();
      cacheMap.forEach((e) => calendar.add(CalendarEntry.fromJSON(e)));
      return calendar;
    } on Exception catch (e) {
      print(e.toString());
      print("Could not retrieve cached version of the calendar !");
      return Calendar();
    }
  }

  /// Save new version of the calendar to the cache
  Future<void> saveCache(CalendarProfile profile, Calendar calendar) async {
    try {
      final entries =
          List.generate(calendar.length, (i) => calendar[i].toJSON());
      final entriesString = jsonEncode(entries);

      final file = await _getCacheFile(profile);
      file.writeAsStringSync(entriesString);
    } on Exception catch (c) {
      print(c.toString());
      print("Could not cache calendar !");
    }
  }
}
