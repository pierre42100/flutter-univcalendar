/// Calendar ID
///
/// Contains and return the ID of a calendar

abstract class CalendarID {

  /// Default constructor
  const CalendarID();

  /// Instantiate calendar ID from a [string]
  CalendarID.fromString(String string);

  /// Return the ID of the calendar in a showable way
  String toDisplayString();

}