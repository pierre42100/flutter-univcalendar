import 'package:calendar_view/calendar_view.dart';
import 'package:univ_calendar_app/calendar_id.dart';
import 'package:univ_calendar_app/colors_set.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/profiles_list.dart';

/// Application preferences
///
/// Contains all the current preferences of the application
///
/// @author Pierre HUBERT

class CalendarProfile {
  final int num;
  final CalendarID calendarID;
  String name;
  int? lastUpdate;
  TileColor? mainColor;
  bool shown;

  CalendarProfile({
    required this.num,
    required this.calendarID,
    required this.name,
    this.lastUpdate = 0,
    this.mainColor,
    this.shown = true,
  }) {
    // Init last update to now by default
    if (this.lastUpdate == 0)
      lastUpdate = DateTime.now().millisecondsSinceEpoch;
  }

  CalendarProfile.fromJSON(Map<String, dynamic> m)
      : this.num = m["num"],
        calendarID = Config.get().calendarIDInitializer(m["calendarID"]),
        name = m["name"],
        lastUpdate = m["lastUpdate"],
        mainColor = (m.containsKey("mainColor") && m["mainColor"] != null)
            ? TileColor.fromJson(m["mainColor"])
            : null,
        this.shown = m.containsKey("shown") ? m["shown"] : true;

  Map<String, dynamic> toJSON() {
    return {
      "num": num,
      "calendarID": calendarID.toString(),
      "name": name,
      "lastUpdate": lastUpdate,
      "mainColor": mainColor == null ? null : mainColor!.toJson(),
      "shown": shown
    };
  }

  int get daysSinceLastUpdate =>
      DateTime.fromMillisecondsSinceEpoch(lastUpdate!)
          .difference(DateTime.now())
          .inDays
          .abs();

  bool get isCalendarTooOld => daysSinceLastUpdate > 7;

  TileColor get defaultTileColor => num == 0
      ? Config.get().defaultTilesColor
      : ColorsSet[(num - 1) % ColorsSet.length];

  // Route user to default tile
  TileColor get actualDefaultTileColor {
    final prefColor = mainColor ?? TileColor.invalid();

    return TileColor(
        bg: prefColor.bg ?? defaultTileColor.bg,
        fg: prefColor.fg ?? defaultTileColor.fg);
  }
}

class MinMaxHours {
  final int min;
  final int max;

  MinMaxHours({required this.min, required this.max});

  bool get isNull => min < 0 || max < 0;
}

class DaysPreferences {
  bool? monday;
  bool? tuesday;
  bool? wednesday;
  bool? thursday;
  bool? friday;
  bool? saturday;
  bool? sunday;

  DaysPreferences({
    this.monday = true,
    this.tuesday = true,
    this.wednesday = true,
    this.thursday = true,
    this.friday = true,
    this.saturday = true,
    this.sunday = true,
  });

  /// Construct a [DayPreferences] with all days disabled
  DaysPreferences.allFalse() {
    for (int i = 1; i < 8; i++) setDayEnabled(i, false);
  }

  /// Construct a [DayPreferences] with all days enabled
  DaysPreferences.allTrue() {
    for (int i = 1; i < 8; i++) setDayEnabled(i, true);
  }

  /// Construct an inverted days preferences
  DaysPreferences.inverted(DaysPreferences prefs) {
    for (int i = 1; i < 8; i++) setDayEnabled(i, !prefs.getDay(i)!);
  }

  int get numberEnabledDays =>
      (monday! ? 1 : 0) +
      (tuesday! ? 1 : 0) +
      (wednesday! ? 1 : 0) +
      (thursday! ? 1 : 0) +
      (friday! ? 1 : 0) +
      (saturday! ? 1 : 0) +
      (sunday! ? 1 : 0);

  /// Get the current state of a day
  bool? getDay(int numDay) {
    switch (numDay) {
      case DateTime.monday:
        return monday;

      case DateTime.tuesday:
        return tuesday;

      case DateTime.wednesday:
        return wednesday;

      case DateTime.thursday:
        return thursday;

      case DateTime.friday:
        return friday;

      case DateTime.saturday:
        return saturday;

      case DateTime.sunday:
        return sunday;

      default:
        throw Exception("Invalid day number!");
    }
  }

  /// Switch the state of a day (1 = monday / 7 = sunday)
  void setDayEnabled(int numDay, bool enabled) {
    switch (numDay) {
      case DateTime.monday:
        monday = enabled;
        break;

      case DateTime.tuesday:
        tuesday = enabled;
        break;

      case DateTime.wednesday:
        wednesday = enabled;
        break;

      case DateTime.thursday:
        thursday = enabled;
        break;

      case DateTime.friday:
        friday = enabled;
        break;

      case DateTime.saturday:
        saturday = enabled;
        break;

      case DateTime.sunday:
        sunday = enabled;
        break;

      default:
        throw Exception("Invalid day number!");
    }
  }

  /// Get calendar view enabled days
  List<WeekDays> get enabledDays {
    final days = List<WeekDays>.empty(growable: true);

    if (monday == true) days.add(WeekDays.monday);
    if (tuesday == true) days.add(WeekDays.tuesday);
    if (wednesday == true) days.add(WeekDays.wednesday);
    if (thursday == true) days.add(WeekDays.thursday);
    if (friday == true) days.add(WeekDays.friday);
    if (saturday == true) days.add(WeekDays.saturday);
    if (sunday == true) days.add(WeekDays.sunday);

    return days;
  }
}

class GlobalCalendarBounds {
  final MinMaxHours hours;
  final DaysPreferences days;

  GlobalCalendarBounds({
    required this.hours,
    required this.days,
  });

  bool get isNull => hours.isNull;
}

class ApplicationPreferences {
  final ProfilesList profiles;
  final bool? autoColorCalendar;
  final int firstHour;
  final int lastHour;
  final double heightSingleDay;
  final double heightFullWeek;
  final DaysPreferences daysPreferences;
  final bool hideOptimizationHint;
  final int calendarShifting;

  ApplicationPreferences({
    required this.profiles,
    required this.autoColorCalendar,
    required this.firstHour,
    required this.lastHour,
    required this.heightSingleDay,
    required this.heightFullWeek,
    required this.daysPreferences,
    required this.hideOptimizationHint,
    required this.calendarShifting,
  });

  MinMaxHours get minMaxHours => MinMaxHours(min: firstHour, max: lastHour);

  bool get hasShifting => calendarShifting != 0;

  bool get hasSetupAutoColor => autoColorCalendar != null;

  bool get hasAtLeastOneCalendar => profiles.length > 0;

  bool hasProfile(int num) => profiles.hasProfile(num);

  int get highestProfilesNumber => profiles.highestProfileNum;

  ProfilesList get enabledProfiles =>
      ProfilesList()..addAll(profiles.where((b) => b.shown).toList());
}
