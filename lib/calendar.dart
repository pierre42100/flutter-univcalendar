import 'dart:collection';

import 'package:univ_calendar_app/calendar_entry.dart';

/// Calendar object
///
/// Contains all the entries of a calendar
///
/// @author Pierre HUBERT

class Calendar extends ListBase<CalendarEntry> {
  List<CalendarEntry?> l = [];

  set length(int newLength) {
    l.length = newLength;
  }

  int get length => l.length;

  @override
  CalendarEntry operator [](int index) => l[index]!;

  @override
  void operator []=(int index, CalendarEntry value) {
    l[index] = value;
  }

  /// Get the list of entries of the calendar for a [day]
  ///
  /// Returns the list found [CalendarEntry]
  Calendar getOfDay(DateTime day) {
    Calendar list = Calendar();

    forEach((f) {
      if (f.start.day == day.day &&
          f.start.month == day.month &&
          f.start.year == day.year) list.add(f);
    });

    return list;
  }

  /// Get all the calendar entries older than a given date
  Calendar getBefore(DateTime day) {
    Calendar list = Calendar();

    forEach((f) {
      if (f.end.compareTo(day) < 0) list.add(f);
    });

    return list;
  }

  /// Get all the calendar entries newer than a given date
  Calendar getAfter(DateTime day) {
    Calendar list = Calendar();

    forEach((f) {
      if (f.end.compareTo(day) > 0) list.add(f);
    });

    return list;
  }

  /// Get the first start date of the list
  DateTime? firstDate() {
    if (length == 0) return null;
    return this[0].start;
  }

  /// Get the last end date of the list
  DateTime? lastDate() {
    if (length == 0) return null;
    return this[length - 1].end;
  }

  /// Get first hour of calendar
  int firstHour(int defaultH) {
    int first = defaultH;

    if (this.length > 0) first = this[0].start.hour;

    forEach((e) {
      if (e.start.hour < first) first = e.start.hour;
    });
    return first;
  }

  /// Get last hour of calendar
  int lastHour(int defaultH) {
    int last = defaultH;

    if (this.length > 0) last = this[0].end.hour;

    forEach((e) {
      if (e.end.hour > last) last = e.end.hour;
    });
    return last;
  }

  /// Merge this calendar with another one (skipping duplicate entries)
  void mergeWith(Calendar calendar) {
    addAll(
      // Select entries not present in this calendar
      calendar.where(
        // Check if the entry is not present in the current calendar
        (newEntry) {
          var isNew = true;

          forEach((currentEntry) {
            if (currentEntry.equals(newEntry)) isNew = false;
          });

          return isNew;
        },
      ),
    );
  }

  /// Get a calendar where all the entries have been shifted of an amount of hours
  Calendar shift(int calendarShifting) {
    final calendar = Calendar();

    forEach((entry) {
      calendar.add(CalendarEntry.shifted(entry, calendarShifting));
    });

    return calendar;
  }

  /// Check if any of the calendar entries is set for a precise week day
  ///
  /// 1 = Monday
  /// 7 = Sunday
  bool hasWeekDay(int dayNumber) {
    var containsDay = false;

    forEach((entry) {
      if (entry.start.weekday == dayNumber) containsDay = true;
    });

    return containsDay;
  }

  /// Count remaining time of similar courses remaining after a given entry
  Duration countRemainingTime(CalendarEntry entry) {
    int mins = 0;

    getAfter(entry.end).forEach((other) {
      if (entry.summary == other.summary) {
        mins += other.duration.inMinutes;
      }
    });

    return Duration(minutes: mins);
  }
}
