import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:univ_calendar_app/calendar.dart';
import 'package:univ_calendar_app/colors_set.dart';
import 'package:univ_calendar_app/config.dart';

import 'calendar_entry.dart';

/// Colors manager
///
/// Allows to customize the color of the events
///
/// @author Pierre HUBERT

class _ColorEntry {
  String summary;
  Color? textColor;
  Color? backgroundColor;

  // ignore: unused_element
  _ColorEntry({required String this.summary, this.textColor, this.backgroundColor});

  _ColorEntry.fromJson(Map<String, dynamic> json)
      : summary = json["summary"],
        textColor = json["textColor"] == null ? null : Color(json["textColor"]),
        backgroundColor = json["backgroundColor"] == null
            ? null
            : Color(json["backgroundColor"]);

  Map<String, dynamic> toJson() {
    return {
      "summary": summary,
      "textColor": textColor == null ? null : textColor!.value,
      "backgroundColor": backgroundColor == null ? null : backgroundColor!.value
    };
  }

  bool get isNull => textColor == null && backgroundColor == null;
}

const _paletteFile = "colors_palette.json";

class ColorManager {
  /// One instance for all app
  static ColorManager? _manager;

  factory ColorManager() {
    if (_manager == null) _manager = ColorManager._internal();

    return _manager!;
  }

  ColorManager._internal();

  late List<_ColorEntry> _entries;

  /// Get entries file
  Future<File> _getEntriesFile() async {
    final appDir = await getApplicationDocumentsDirectory();
    return File("${appDir.path}/$_paletteFile");
  }

  /// Read colors from file
  Future<List<_ColorEntry>> _readEntries() async {
    try {
      //Check and access file
      final file = await _getEntriesFile();

      if (!await file.exists()) {
        print("Colors palette file does not exists yet.");
        return [];
      }

      //Read and decode file
      final fileContent = file.readAsStringSync();
      final List json = jsonDecode(fileContent);
      List<_ColorEntry> entries = [];
      json.forEach((e) => entries.add(_ColorEntry.fromJson(e)));
      return entries;
    } on Exception catch (e) {
      print("Could not initialize colors!");
      print(e.toString());
      return [];
    }
  }

  /// Write colors to file
  Future<void> _writeEntries(List<_ColorEntry> list) async {
    try {
      final file = await _getEntriesFile();
      final List<Map<String, dynamic>> entries =
          List.generate(list.length, (i) => list[i].toJson());
      file.writeAsStringSync(jsonEncode(entries));
    } on Exception catch (e) {
      print("Could not write entries to file!");
      print(e.toString());
    }
  }

  /// Search for a color in the list for an [entry]. Returns -1 if not found
  int _search(CalendarEntry entry) {
    for (int i = 0; i < _entries.length; i++)
      if (_entries[i].summary == entry.summary) return i;

    return -1;
  }

  /// Update an entry
  Future<void> _updateEntry(
      CalendarEntry entry, Color? color, bool isText) async {
    if (_search(entry) == -1) _entries.add(_ColorEntry(summary: entry.summary));

    int num = _search(entry);

    if (isText)
      _entries[num].textColor = color;
    else
      _entries[num].backgroundColor = color;

    _writeEntries(_entries);
  }

  /// Initialize colors
  Future<void> initialize() async {
    _entries = await _readEntries();
  }

  /// Delete all colors
  Future<void> reset() async {
    _entries = [];
    _writeEntries(_entries);
  }

  /// Get the background [Color] for a specified [entry]
  Color? getBackgroundColor(CalendarEntry entry, {TileColor? def}) {
    final pos = _search(entry);
    if (pos == -1 || _entries[pos].backgroundColor == null)
      return def == null ? Config.get().defaultBackgroundColor : def.bg;
    else
      return _entries[pos].backgroundColor;
  }

  /// Get the text [Color] for a specified [entry]
  Color? getTextColor(CalendarEntry entry, {TileColor? def}) {
    final pos = _search(entry);
    if (pos == -1 || _entries[pos].textColor == null)
      return def == null ? Config.get().defaultTextColor : def.fg;
    else
      return _entries[pos].textColor;
  }

  /// Get the color for a calendar
  TileColor getEntryColors(CalendarEntry entry, {bool allowNull = false}) {
    // Return null object if possible
    final def = allowNull ? TileColor(bg: null, fg: null) : null;

    return TileColor(
      bg: getBackgroundColor(entry, def: def),
      fg: getTextColor(entry, def: def),
    );
  }

  /// Set new color for a specified [entry]
  ///
  /// Set [color] to null to reset
  Future<void> setColor(CalendarEntry entry, TileColor color) async {
    await _updateEntry(entry, color.bg, false);
    await _updateEntry(entry, color.fg, true);
  }

  /// Set a default color for each calendar entry that does not have a color yet
  Future<void> fillMissingColorsWithRandom(Calendar calendar) async {
    calendar.forEach((entry) {
      if (_search(entry) == -1 ||
          (_search(entry) != -1 && _entries[_search(entry)].isNull))
        // Set a default color
        setColor(entry, pickRandomColor());
    });
  }
}
