/// Calendar widget
///
/// @author Pierre HUBERT

import 'package:calendar_view/calendar_view.dart';
import 'package:flutter/material.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar_entry.dart';
import 'package:univ_calendar_app/calendars_list.dart';
import 'package:univ_calendar_app/color_manager.dart';
import 'package:univ_calendar_app/profiles_list.dart';
import 'package:univ_calendar_app/utils.dart';

typedef OnDateChange = void Function(DateTime newDate);

typedef CalendarController = EventController<CalendarEntryWithNumber>;

class CalendarWidget extends StatefulWidget {
  final CalendarsList calendars;
  final ProfilesList profiles;
  final OnDateChange onDateChange;
  final ApplicationPreferences preferences;
  final CalendarController controller;

  CalendarWidget({
    required this.calendars,
    required this.profiles,
    required this.onDateChange,
    required this.preferences,
    required this.controller,
  }) : assert(calendars.length == profiles.length);

  @override
  State<StatefulWidget> createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget> {
  final ColorManager _colorManager = ColorManager();
  bool _ready = false;

  CalendarsList get _calendars => widget.calendars;

  /// Close currently open dialog
  void _closeDialog() {
    Navigator.pop(context);
  }

  /// Update loading progress visibility
  void _setReady(bool ready) {
    setState(() {
      _ready = ready;
    });
  }

  void _showEntryInfo(
      BuildContext context, CalendarEntry entry, int numCalendar) async {
    try {
      // Count remaining time
      final remainingTime = _calendars[numCalendar].countRemainingTime(entry);
      final remainingHours = remainingTime.inHours;
      final remainingMinutes = remainingTime.inMinutes % 60;

      await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Informations"),
            content: SingleChildScrollView(
              child: Text(
                "Début : ${entry.start}\n" +
                    "Fin : ${entry.end}\n" +
                    "Résumé : ${entry.summary}\n" +
                    "Lieu : ${entry.location}\n" +
                    "Description : ${entry.description}\n" +
                    "Temps restant : ${remainingHours}h" +
                    (remainingMinutes >= 10
                        ? remainingMinutes.toString()
                        : "0$remainingMinutes"),
              ),
            ),
            actions: <Widget>[
              TextButton(
                onPressed: _closeDialog,
                child: Text("OK"),
              )
            ],
          );
        },
      );
    } catch (e, s) {
      print("$e $s");
    }
  }

  /// Initialize color manager
  Future<void> _initializeColorManager() async {
    _setReady(false);
    await _colorManager.initialize();

    _setReady(true);
  }

  /// Configure entry colors
  void _configureEntryColors(BuildContext context, CalendarEntry entry) async {
    final newColors = await pickColors(
      context: context,
      currentColors: _colorManager.getEntryColors(entry, allowNull: true),
      sampleText: entry.summary,
    );

    if (newColors == null) return;

    _setReady(false);
    await _colorManager.setColor(entry, newColors);
    _setReady(true);
  }

  /// We need this to load color palette
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (mounted) _initializeColorManager();
  }

  @override
  Widget build(BuildContext context) {
    if (!_ready)
      return Center(
        child: CircularProgressIndicator(color: Colors.black),
      );

    if (showFullWeek(context, widget.preferences))
      return WeekView(
        controller: widget.controller,
        weekDays: widget.preferences.daysPreferences.enabledDays,
        timeLineStringBuilder: _timeStringBuilder,
        timeLineWidth: 35,
        timeLineBuilder: (d) => Center(
            child: Text(
          _timeStringBuilder(d),
          style: TextStyle(fontSize: 10),
        )),
        onPageChange: (d, i) => {widget.onDateChange(d)},
        startHour: widget.preferences.firstHour > 0
            ? widget.preferences.firstHour - 1
            : 0,
        endHour: widget.preferences.lastHour < 24
            ? widget.preferences.lastHour + 1
            : 24,
        heightPerMinute: widget.preferences.heightFullWeek * 1.5,
        onEventDoubleTap: (events, dt) {
          if (events.isNotEmpty) {
            final event = events[0].event! as CalendarEntryWithNumber;
            _showEntryInfo(context, event.entry, event.number);
          }
        },
        onEventLongTap: (events, dt) {
          if (events.isNotEmpty) {
            final event = events[0].event! as CalendarEntryWithNumber;
            _configureEntryColors(context, event.entry);
          }
        },
        eventTileBuilder: _defaultEventTileBuilder,
        weekTitleHeight: 20,
        weekDayBuilder: (a) => Center(
            child: Text(
          "${getNiceDate(a, days: true, month: false)}",
          style: TextStyle(fontSize: 10),
        )),
        weekNumberBuilder: (a) => Text(""),
        headerStringBuilder: (d, {secondaryDate}) =>
            "${_dayStringBuilder(d)} > ${_dayStringBuilder(secondaryDate!)}",
      );

    return DayView(
      controller: widget.controller,
      dateStringBuilder: _dayStringBuilder,
      timeStringBuilder: _timeStringBuilder,
      timeLineBuilder: (d) => Center(
          child: Text(
        _timeStringBuilder(d),
        style: TextStyle(fontSize: 10),
      )),
      timeLineWidth: 25,
      onPageChange: (d, i) => {widget.onDateChange(d)},
      startHour: widget.preferences.firstHour > 0
          ? widget.preferences.firstHour - 1
          : 0,
      endHour: widget.preferences.lastHour < 24
          ? widget.preferences.lastHour + 1
          : 24,
      heightPerMinute: widget.preferences.heightSingleDay * 1.5,
      onEventDoubleTap: (events, dt) {
        if (events.isNotEmpty) {
          final event = events[0].event! as CalendarEntryWithNumber;
          _showEntryInfo(context, event.entry, event.number);
        }
      },
      onEventLongTap: (events, dt) {
        if (events.isNotEmpty) {
          final event = events[0].event! as CalendarEntryWithNumber;
          _configureEntryColors(context, event.entry);
        }
      },
      eventTileBuilder: _defaultEventTileBuilder,
    );
  }

  String _timeStringBuilder(DateTime date, {DateTime? secondaryDate}) =>
      "${date.hour.toString().padLeft(2, "0")}h";

  String _dayStringBuilder(DateTime date, {DateTime? secondaryDate}) =>
      getNiceDate(date, days: true, month: true);

  Widget _defaultEventTileBuilder(
    DateTime date,
    List<CalendarEventData<Object?>> events,
    Rect boundary,
    DateTime startDuration,
    DateTime endDuration,
  ) {
    return DefaultEventTile(
      date: date,
      events: events,
      boundary: boundary,
      startDuration: startDuration,
      endDuration: endDuration,
      profiles: widget.profiles,
      colorsManager: _colorManager,
    );
  }
}

class DefaultEventTile<T> extends StatelessWidget {
  const DefaultEventTile(
      {required this.date,
      required this.events,
      required this.boundary,
      required this.startDuration,
      required this.endDuration,
      required this.profiles,
      required this.colorsManager});

  final DateTime date;
  final List<CalendarEventData<T>> events;
  final Rect boundary;
  final DateTime startDuration;
  final DateTime endDuration;
  final List<CalendarProfile> profiles;
  final ColorManager colorsManager;

  @override
  Widget build(BuildContext context) {
    if (events.isNotEmpty) {
      final event = events[0];
      final data = event.event! as CalendarEntryWithNumber;

      final textStyle = TextStyle(
          fontSize: 12,
          color: colorsManager.getTextColor(data.entry,
                  def: profiles[data.number].actualDefaultTileColor) ??
              event.color);

      return RoundedEventTile(
        borderRadius: BorderRadius.circular(10.0),
        title: "${data.entry.summary}\n${data.entry.location}",
        totalEvents: events.length - 1,
        description: event.description,
        padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
        backgroundColor: colorsManager.getBackgroundColor(data.entry,
                def: profiles[data.number].actualDefaultTileColor) ??
            event.color,
        margin: EdgeInsets.all(2.0),
        titleStyle: textStyle.merge(event.titleStyle),
        descriptionStyle: textStyle.merge(event.descriptionStyle),
      );
    } else {
      return SizedBox.shrink();
    }
  }
}
