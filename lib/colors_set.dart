import 'dart:math';

import 'package:flutter/material.dart';

/// Colors set
///
/// Contains a list of preset colors

const INVALID_COLOR = -2;

class TileColor {
  final Color? bg;
  final Color? fg;

  const TileColor({required this.bg, required this.fg});

  const TileColor.invalid()
      : fg = null,
        bg = null;

  TileColor.fromJson(Map<String, dynamic> m)
      : fg = m["fg"] != INVALID_COLOR ? Color(m['fg']) : null,
        bg = m["bg"] != INVALID_COLOR ? Color(m['bg']) : null;

  Map<String, dynamic> toJson() {
    return {
      "bg": (bg == null ? INVALID_COLOR : bg!.value),
      "fg": (fg == null ? INVALID_COLOR : fg!.value)
    };
  }

  bool get isNull => bg == null || fg == null;
}

const ColorsSet = [
  // Custom colors
  TileColor(bg: Color(0xFF795548), fg: Colors.white),
  TileColor(bg: Color(0xFF673ab7), fg: Colors.white),
  TileColor(bg: Color(0xFF009688), fg: Colors.white),
  TileColor(bg: Color(0xFF131418), fg: Colors.white),

  // Colors from the palette
  TileColor(bg: Colors.pink, fg: Colors.white),
  TileColor(bg: Colors.pinkAccent, fg: Colors.white),
  TileColor(bg: Colors.red, fg: Colors.white),
  TileColor(bg: Colors.redAccent, fg: Colors.white),
  TileColor(bg: Colors.deepOrange, fg: Colors.white),
  TileColor(bg: Colors.deepOrangeAccent, fg: Colors.white),
  TileColor(bg: Colors.orange, fg: Colors.black),
  TileColor(bg: Colors.orangeAccent, fg: Colors.black),
  TileColor(bg: Color(0xFFE65100), fg: Colors.white),
  TileColor(bg: Colors.amber, fg: Colors.black),
  TileColor(bg: Color(0xFFFF6F00), fg: Colors.white),
  TileColor(bg: Colors.amberAccent, fg: Colors.black),
  TileColor(bg: Colors.yellow, fg: Colors.black),
  TileColor(bg: Colors.yellowAccent, fg: Colors.black),
  TileColor(bg: Colors.lime, fg: Colors.black),
  TileColor(bg: Color(0xFF827717), fg: Colors.white),
  TileColor(bg: Colors.limeAccent, fg: Colors.black),
  TileColor(bg: Colors.lightGreen, fg: Colors.black),
  TileColor(bg: Color(0xFF33691E), fg: Colors.white),
  TileColor(bg: Colors.lightGreenAccent, fg: Colors.black),
  TileColor(bg: Colors.green, fg: Colors.white),
  TileColor(bg: Colors.greenAccent, fg: Colors.black),
  TileColor(bg: Colors.teal, fg: Colors.white),
  TileColor(bg: Color(0xFF004D40), fg: Colors.white),
  TileColor(bg: Colors.tealAccent, fg: Colors.black),
  TileColor(bg: Colors.cyan, fg: Colors.black),
  TileColor(bg: Color(0xFF006064), fg: Colors.white),
  TileColor(bg: Colors.lightBlue, fg: Colors.black),
  TileColor(bg: Color(0xFF01579B), fg: Colors.white),
  TileColor(bg: Colors.lightBlueAccent, fg: Colors.black),
  TileColor(bg: Colors.blue, fg: Colors.white),
  TileColor(bg: Color(0xFF0D47A1), fg: Colors.white),
  TileColor(bg: Colors.blueAccent, fg: Colors.white),
  TileColor(bg: Colors.indigo, fg: Colors.white),
  TileColor(bg: Color(0xFF1A237E), fg: Colors.white),
  TileColor(bg: Colors.indigoAccent, fg: Colors.white),
  TileColor(bg: Colors.purple, fg: Colors.white),
  TileColor(bg: Color(0xFF4A148C), fg: Colors.white),
  TileColor(bg: Colors.purpleAccent, fg: Colors.white),
  TileColor(bg: Colors.deepPurple, fg: Colors.white),
  TileColor(bg: Color(0xFF311B92), fg: Colors.white),
  TileColor(bg: Colors.deepPurpleAccent, fg: Colors.white),
  TileColor(bg: Colors.blueGrey, fg: Colors.white),
  TileColor(bg: Color(0xFF263238), fg: Colors.white),
  TileColor(bg: Colors.brown, fg: Colors.white),
  TileColor(bg: Color(0xFF3E2723), fg: Colors.white),
  TileColor(bg: Colors.grey, fg: Colors.black),
  TileColor(bg: Color(0xFF212121), fg: Colors.white),
];

/// Pick a random color from set
TileColor pickRandomColor() {
  return ColorsSet[Random().nextInt(ColorsSet.length)];
}
