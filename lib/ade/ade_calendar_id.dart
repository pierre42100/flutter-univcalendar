import 'package:univ_calendar_app/calendar_id.dart';

/// ADELB calendar ID
///
/// @author Pierre HUBERT

class ADECalendarID extends CalendarID {
  String? calendarId;
  String? projectId;

  ADECalendarID(this.calendarId, [this.projectId]);

  ADECalendarID.fromString(String string) {
    if (!string.startsWith("#")) {
      this.calendarId = string;
      this.projectId = null;
    } else {
      final split = string.split("#");
      this.projectId = split[1];
      this.calendarId = split[2];
    }
  }

  @override
  String toString() {
    if (projectId == null)
      return calendarId.toString();
    else
      return "#$projectId#$calendarId";
  }

  @override
  String toDisplayString() {
    return calendarId.toString();
  }

  bool get valid {
    return calendarId != null &&
        calendarId!.length >= 1 &&
        RegExp("^[0-9,]+\$").hasMatch(calendarId!);
  }

  static bool isValid(String s) {
    return s.length >= 1 && RegExp("^[0-9,]+\$").hasMatch(s);
  }
}
