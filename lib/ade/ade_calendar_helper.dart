import 'package:univ_calendar_app/ade/ade_calendar_id.dart';
import 'package:univ_calendar_app/app_preferences.dart';
import 'package:univ_calendar_app/calendar.dart';
import 'package:univ_calendar_app/calendar_helper.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/ical/ical_calendar_helper.dart';

import 'ade_config.dart';

/// Calendar helper
///
/// @author Pierre HUBERT

class ADECalendarHelper extends CalendarHelper {
  /// Download a calendar from the server
  ///
  /// The [calendarID] is used to check which calendar has to be retrieved
  ///
  /// It returns the list of calendar entries in case of success or null in
  /// case of failure
  Future<Calendar?> download(CalendarProfile profile) async {
    //Get the dates to parse
    final currDate = DateTime.now();
    final endDate = currDate.add(new Duration(days: 365));

    //Prepare the request
    final path = "/jsp/custom/modules/plannings/anonymous_cal.jsp";
    ADEConfig serverConfig = Config.get() as ADEConfig;
    ADECalendarID id = profile.calendarID as ADECalendarID;
    final args = {
      "resources": id.calendarId,
      "projectId": id.projectId ?? serverConfig.serverProjectVersion.toString(),
      "calType": "ical",
      "firstDate": _getDateForRequest(currDate),
      "lastDate": _getDateForRequest(endDate),
    };

    // Start as required an http or an https request
    var url;
    if (ADEConfig.get().serverIsSecure)
      url = Uri.https(ADEConfig.get().serverAddress, path, args);
    else
      url = Uri.http(ADEConfig.get().serverAddress, path, args);
    try {
      return IcalCalendarHelper.downloadURL(
        url.toString(),
        shifting: ADEConfig.get().serverHoursDifference,
        needFullDaySupport: false,
        needLangSupport: false,
      );
    } on Exception catch (e) {
      print(e.toString());
      return null;
    }
  }

  /// Get a date for the request on the server
  ///
  /// [date] is the date to request to the server
  ///
  /// Returns the corresponding string
  String _getDateForRequest(DateTime date) {
    return date.year.toString() +
        "-" +
        date.month.toString() +
        "-" +
        date.day.toString();
  }
}
