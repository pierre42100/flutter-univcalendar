import 'package:univ_calendar_app/ade/ade_calendar_id.dart';

/// ADE client utilities
///
/// @author Pierre HUBERT

/// Extract from a given [url] the ID of the related calendar
ADECalendarID? extractResourceID(String url) {
  // Calendar ID
  final RegExp exp = RegExp(r"resources=[0-9,]+");
  final match = exp.firstMatch(url);
  if (match == null) return null;
  final string = match.group(0)!;
  final calendarID = string.substring(string.indexOf('=') + 1);

  // Project ID
  final RegExp expProj = RegExp(r"projectId=[0-9]+");
  final matchProj = expProj.firstMatch(url);
  if (matchProj == null) return null;
  final stringProj = matchProj.group(0)!;
  final projID = stringProj.substring(stringProj.indexOf('=') + 1);


  return ADECalendarID(calendarID, projID);
}
