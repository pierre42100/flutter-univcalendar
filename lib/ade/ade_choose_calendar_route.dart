import 'dart:async';

import 'package:flutter/material.dart';
import 'package:univ_calendar_app/ade/ade_calendar_id.dart';
import 'package:univ_calendar_app/ade/ade_config.dart';
import 'package:univ_calendar_app/ade/ade_utils.dart';
import 'package:univ_calendar_app/calendar_id.dart';
import 'package:univ_calendar_app/choose_calendar_route.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/stats.dart';
import 'package:univ_calendar_app/utils.dart';

class ADEChooseCalendarRoute extends ChooseCalendarRoute {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(Config.get().appName),
      ),
      body: _ChooseCalendarBody(
        completer: completer,
      ),
    );
  }
}

class _ChooseCalendarBody extends StatefulWidget {
  final Completer<CalendarID> completer;

  const _ChooseCalendarBody({Key? key, required this.completer})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChooseCalendarBodyState();
}

class _ChooseCalendarBodyState extends State<_ChooseCalendarBody> {
  bool _isEntryValid = true;
  String _currNumber = "";

  /// Entry updated
  void _entryUpdated(String s) {
    if (!ADECalendarID.isValid(s)) {
      setState(() {
        _isEntryValid = false;
      });
      return;
    }

    // Save new value
    _currNumber = s;
    setState(() {
      _isEntryValid = true;
    });
  }

  /// Validate user input
  Future<void> _validateEntry() async {
    print("Validate manual input");
    if (!ADECalendarID.isValid(_currNumber)) return;

    await setCalendarID(ADECalendarID(_currNumber));
  }

  /// Set (save calendar ID)
  Future<void> setCalendarID(ADECalendarID id) async {
    reportNewCalendar(app: Config.get().appName, cal: id.toString());
    widget.completer.complete(id);
  }

  /// Use the QRCode method
  Future<void> _scanQRCode() async {
    try {
      final qrCode = await scanQrCode(context);

      if (qrCode == null) {
        throw "Barcode null!";
      }

      reportQrScan(url: qrCode);

      //Analyze QRCode
      //Check if it is the right URL
      final possiblePrefix = [
        "https://${ADEConfig.get().serverAddress}/",
        "http://${ADEConfig.get().serverAddress}/"
      ];

      if (!possiblePrefix.any((e) => qrCode.startsWith(e)))
        throw "Not the right prefix $qrCode !";

      //Extract calendar ID
      final calendarID = extractResourceID(qrCode);

      if (calendarID == null || !calendarID.valid)
        throw "Could not extract calendar ID!";

      setCalendarID(calendarID);
    } catch (e, c) {
      print(c.toString());
      print("Could not scan barcode!");

      //Notify user
      showPopup(context, "Erreur lors du scan du QRCode !");
    }
  }

  Widget _buildCalendarPickerInstructions() {
    ADEConfig conf = Config.get() as ADEConfig;
    if (!conf.hasCalendarPicker) return Container();

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Text(
              "Choix du calendrier depuis l'interface web",
              style: TextStyle(fontSize: 18.0),
            ),
            Text(
              "Connectez-vous à votre compte universitaire pour configurer un calendrier",
              textAlign: TextAlign.justify,
            ),
            ElevatedButton(
              child: Text(
                "Choix du calendrier",
                style: TextStyle(
                  color: conf.defaultTextColor,
                ),
              ),
              onPressed: () => _openCalendarPicker(),
              style:
                  ElevatedButton.styleFrom(backgroundColor: conf.appBarColor),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _openCalendarPicker() async {
    /*final calendarID = await adePickCalendar(context); // NOT IMPLEMENTED ANYMORE

    if (!calendarID.valid) return;

    // save calendar ID
    setCalendarID(calendarID);*/
  }

  /// Manual way
  Widget _buildManualSelectionInstructions() {
    ADEConfig conf = Config.get() as ADEConfig;

    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            conf.hasCalendarPicker
                ? Text(
                    "Saisie manuelle du numéro de calendrier",
                    style: TextStyle(fontSize: 18.0),
                  )
                : Container(),
            _buildInstructions(),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text("-- CHOIX DU CALENDRIER --"),
            ),
            ElevatedButton(
              style: ButtonStyle(
                  backgroundColor: WidgetStatePropertyAll(
                      Config.get().defaultBackgroundColor),
                  foregroundColor:
                      WidgetStatePropertyAll(Config.get().defaultTextColor)),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.qr_code),
                    Text("Scan du QrCode"),
                  ],
                ),
              ),
              onPressed: () {
                _scanQRCode();
              },
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text("--- OU ---"),
            ),
            _buildNumberForm(),
          ],
        ),
      ),
    );
  }

  /// Help notice
  Widget _buildInstructions() {
    ADEConfig conf = Config.get() as ADEConfig;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Center(
            child: Text("Instructions :"),
          ),
          Text("1. Ouvrez l'interface web de votre calendrier (" +
              conf.serverConfigurationURL +
              ") et accédez au calendrier que vous souhaitez installer dans cette application."),
          Text(
              "2. En bas à gauche de la page, sous 'Options' cliquez sur le bouton d'export de calendrier (deuxième bouton en partant de la gauche)"),
          Text("3. Dans la fenêtre qui apparaît, cliquez sur 'Générer URL'."),
          Text(
              "4. Vous pouvez alors soit flasher le QrCode soit recopier le numéro situé après resources= et avant le &"),
        ],
      ),
    );
  }

  Widget _buildNumberForm() {
    return Container(
      width: 100,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          // Notice
          Text(
            "Numéro du calendrier :",
            textAlign: TextAlign.center,
          ),

          // User text input
          TextField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              hintText: "Numéro",
              errorText: _isEntryValid ? null : "Numéro invalide!",
            ),
            onChanged: _entryUpdated,
          ),

          // Submit button
          Center(
            child: ElevatedButton(
              onPressed: _isEntryValid && ADECalendarID.isValid(_currNumber)
                  ? _validateEntry
                  : null,
              child: Text("Valider"),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        child: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Choix d'un calendrier",
              style: TextStyle(fontSize: 25.0),
            ),
          ),
          _buildCalendarPickerInstructions(),
          _buildManualSelectionInstructions()
        ],
      ),
    ));
  }
}
