import 'package:flutter/widgets.dart';
import 'package:univ_calendar_app/ade/ade_calendar_helper.dart';
import 'package:univ_calendar_app/ade/ade_calendar_id.dart';
import 'package:univ_calendar_app/ade/ade_choose_calendar_route.dart';
import 'package:univ_calendar_app/config.dart';

/// Adelb config
///
/// @author Pierre HUBERT

typedef CalendarPicker = Future<int> Function(BuildContext);

class ADEConfig extends Config {
  final String serverAddress;
  final bool serverIsSecure;
  final String serverConfigurationURL;
  final int serverProjectVersion;
  final int serverHoursDifference;
  final String? initialCalendarPickerURL;
  final String? validableCalendarPickerURL;
  final String? pickerJSPath;
  final String? pickerInitialInstructions;

  ADEConfig({
    required this.serverConfigurationURL,
    required String appName,
    required this.serverAddress,
    required this.serverIsSecure,
    required Color appBarColor,
    required Color defaultTextColor,
    required Color defaultBackgroundColor,
    required this.serverProjectVersion,
    this.serverHoursDifference = 0,
    this.initialCalendarPickerURL,
    this.validableCalendarPickerURL,
    this.pickerJSPath,
    this.pickerInitialInstructions,
  })  :
        super(
            appName: appName,
            appBarColor: appBarColor,
            defaultTextColor: defaultTextColor,
            defaultBackgroundColor: defaultBackgroundColor,
            calendarIDInitializer: (s) {
              return ADECalendarID.fromString(s);
            },
            calendarHelperInitializer: () {
              return ADECalendarHelper();
            },
            chooseCalendarRouteInitializer: () {
              return ADEChooseCalendarRoute();
            });

  bool get hasCalendarPicker =>
      initialCalendarPickerURL != null &&
      validableCalendarPickerURL != null &&
      pickerJSPath != null;

  bool get hasSpecialPickerInstructions => pickerInitialInstructions != null;

  static ADEConfig get() {
    return Config.get() as ADEConfig;
  }
}
