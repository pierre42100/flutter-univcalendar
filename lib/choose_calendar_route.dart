import 'dart:async';

import 'package:flutter/material.dart';
import 'package:univ_calendar_app/calendar_id.dart';

/// Choose calendar route
///
/// @author Pierre HUBERT

abstract class ChooseCalendarRoute extends StatelessWidget {
  /// Completer & future in order to use chooser in a picker fashion moe
  @protected
  final Completer<CalendarID> completer = Completer();

  Future<CalendarID> get future => completer.future;
}
