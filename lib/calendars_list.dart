import 'dart:collection';

import 'package:univ_calendar_app/calendar.dart';

/// Calendars list
///
/// @author Pierre HUBERT

class CalendarsList extends ListBase<Calendar> {
  final _list = <Calendar?>[];

  int get length => _list.length;

  set length(l) => _list.length = l;

  @override
  Calendar operator [](int index) => _list[index]!;

  @override
  void operator []=(int index, Calendar value) => _list[index] = value;

  DateTime get firstDate {
    var date = DateTime.now();

    forEach((c) {
      final curr = c.firstDate();

      if (curr != null && curr.isBefore(date)) date = curr;
    });

    return date;
  }

  DateTime get lastDate {
    var date = DateTime.now();

    forEach((c) {
      final curr = c.lastDate();

      if (curr != null && curr.isAfter(date)) date = curr;
    });

    return date;
  }

  /// Shift calendar
  CalendarsList shift(int shifting) =>
      CalendarsList()..addAll(map((c) => c.shift(shifting)));
}
