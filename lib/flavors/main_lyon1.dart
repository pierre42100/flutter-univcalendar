import 'package:flutter/material.dart';
import 'package:univ_calendar_app/ade/ade_config.dart';

/// Main page for Université Lyon 1
///
/// @author Pierre HUBERT

import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/main.dart';

void main() {
  //Univ Lyon 1 configuration
  Config.setConfig(ADEConfig(
    serverConfigurationURL: "edt.univ-lyon1.fr",
    appName: "Calendrier Université Lyon 1",
    serverAddress: "adelb.univ-lyon1.fr",
    serverIsSecure: true,
    appBarColor: Colors.blueAccent,
    defaultBackgroundColor: Colors.indigo,
    defaultTextColor: Colors.white,
    serverProjectVersion: 0,
    serverHoursDifference: 0,
  ));

  subMain();
}
