import 'package:flutter/material.dart';
import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/ical/ical_config.dart';
import 'package:univ_calendar_app/main.dart';

/// Generic iCal calendar application
///
/// @author Pierre HUBERT

void main() {
  Config.setConfig(IcalConfig(
    appName: "Calendrier",
    appBarColor: Color(0xFF303030),
    defaultTextColor: Colors.white,
    defaultBackgroundColor: Color(0xFF212121),
  ));

  subMain();
}
