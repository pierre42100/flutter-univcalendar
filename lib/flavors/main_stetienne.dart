import 'package:flutter/material.dart';
import 'package:univ_calendar_app/ade/ade_config.dart';

/// Main page for Université de Saint-Etienne (Jean Monnet)
///
/// @author Pierre HUBERT

import 'package:univ_calendar_app/config.dart';
import 'package:univ_calendar_app/main.dart';

void main() {
  //Univ St etienne configuration
  Config.setConfig(ADEConfig(
    serverConfigurationURL: "ent.univ-st-etienne.fr",
    appName: "Calendrier Université Saint Etienne",
    serverAddress: "planning.univ-st-etienne.fr",
    serverIsSecure: true,
    appBarColor: Colors.green[900]!,
    defaultBackgroundColor: Colors.green,
    defaultTextColor: Colors.white,
    serverProjectVersion: 4,
  ));

  subMain();
}
